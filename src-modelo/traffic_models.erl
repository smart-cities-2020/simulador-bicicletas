-module(traffic_models).
% Essa era uma "safe-area" de rascunho...
% Mas agora o código já foi levado pro lugar onde tem q ficar (fork do repo oficial do Simulador)
% Modelo em https://github.com/leonardofl/smart_city_model/blob/master/src/traffic_models.erl
% Testes em https://github.com/leonardofl/smart_city_model/blob/master/src/traffic_models_test.erl
% O código não será mais evoluido neste local (somente no fork supra-citado)


-export([get_speed_bike/9, get_speed_bike_tests/0, get_personal_bike_speed/0]).
%-include_lib("eunit/include/eunit.hrl").

get_personal_bike_speed() ->
    % The sampled speed is retrieved from a previously generated list following a distribution observed in the OD (Pesquisa OrigemDestino) data.
    SampledSpeed = get_next_value_from_speeds_distribution(),
    % What matters in this speeds list is the distribution (a generalized gama, as told us by the fit test done in R).
    % But the speeds in our OD dataset are lower than actual speeds, because we used as distance the euclidean distance from origin to destination - a straight line linking origin and destination. 
    % The mean speed for the OD dataset is 7.5km/h.
    % We considered the Bike Sampa dataset to me more accurate. In this Bike Sampa dataset the mean speed mixed traffic is 10km/h.
    % Therefore, we apply a factor over the sampled speed to correct the offset of the speed distribution.
    FactorOdToBikeSampa = 10/7.5,
    SampledSpeed * FactorOdToBikeSampa.

get_next_value_from_speeds_distribution() ->

    % First, we open the file if it's the first time this function is invoked
    case lists:member(table_personal_speeds, ets:all()) of
        false -> 
            ets:new(table_personal_speeds, [named_table, protected, set, {keypos, 1}]),
            Filename = "personal_speed_distribution.csv",
            {_ok, File} = file:open(Filename, read),
            ets:insert(table_personal_speeds, {file, File});
            % we keep the file in the EST, so we keep implicitly together the pointer to the last read line.
        _ -> nothing_to_do
    end,

    [{_, SpeedsFile}] = ets:lookup(table_personal_speeds, file),
    Line = io:get_line(SpeedsFile, ''), % read next line of the file
    % Obs: if we got at the end of file, the system will crash! But we hope this to not happen!
    {SpeedKmh, _} = string:to_float(Line),
    SpeedKmh/3.6.

% PersonalSpeed: different people have different speeds; each agent must hold a personal speed generated once for the actor. The personal speed must be generated using the function get_personal_bike_speed.
% Length: length of the link (meters)
% Capacity: how many cars the link supports
% NumberBikes: how many bikes are in the link
% NumberCars: how many cars are in the link
% IsCycleway: boolean
% IsCyclelane: boolean
% AltitudeNodeFrom: altitude of the origin node (meters)
% AltitudeNodeTo: altitude of the target node (meters)
get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo) ->
    Freespeed = get_free_speed(PersonalSpeed, Length, IsCycleway, IsCyclelane, AltitudeNodeTo, AltitudeNodeFrom),
    Speed = speed_considering_traffic(Freespeed, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane),
    Speed.



get_free_speed(PersonalSpeed, Length, IsCycleway, IsCyclelane, AltitudeNodeTo, AltitudeNodeFrom) ->

    Inclination = (AltitudeNodeTo - AltitudeNodeFrom) / Length,
    Climb = Inclination > 0.02,
    Descent = Inclination < -0.02,
%    IsMixedTraffic = (not IsCycleway) and (not IsCyclelane),
%    Plane = (not Climb) and (not Descent),

    % Base for the factors (Bike Sampa dataset):
    % Plane cyclelane: 9.6km/h
    % Climb cyclelane: 6.8km/h
    % Descent cyclelane: 15.1km/h
    % Plane Cycleway: 15.5km/h
    CyclelaneFactor = 1.2, % at Bike Sampa, mean speed is 9.6km/h
    CyclewayFactor = 1.5, % at Bike Sampa, mean speed is 15.5km/h
    ClimbFactor = 0.71,
    DescentFactor = 1.57,

    SpeedWithInfra = if
        IsCyclelane -> PersonalSpeed * CyclelaneFactor;
        IsCycleway -> PersonalSpeed * CyclewayFactor;
        true -> PersonalSpeed
    end,

    FreeSpeed = if
        Climb -> SpeedWithInfra * ClimbFactor;
        Descent -> SpeedWithInfra * DescentFactor;
        true -> SpeedWithInfra
    end,

    FreeSpeed.
   







speed_considering_traffic(BaseSpeed, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane) ->

    IsMixedTraffic = (not IsCycleway) and (not IsCyclelane),
    if
        IsMixedTraffic ->
            % cellsize do carro = 7,5 e cellsize da bike = 3
            % cellsize é o comprimento do veículo mais a distância de segurança
            % Considera também que em uma faixa de carro passam duas bikes uma do lado da outra 
            Occupation = NumberBikes/(2.5*2) + NumberCars;
        true -> % ciclovia ou ciclofaixa (é uma bike atrás da outra)
            Occupation = NumberBikes/(2.5*1)
    end,
    SaturatedLink = Occupation >= Capacity,

	Alpha = 1,
	Beta = 1,
    if
        SaturatedLink ->
             1.0;
        true -> % (não saturado) ou (saturado em tráfego misto)
            BaseSpeed * math:pow(1 - math:pow((Occupation / Capacity), Beta), Alpha)
	end.




%%%%%%%%%%%%%%%%%
% TESTES %%%%%%%%
%%%%%%%%%%%%%%%%%
% Para executar o teste:
% erlc traffic_models.erl
% erl -noshell -s traffic_models get_speed_bike_test -s init stop

get_speed_bike_tests() ->
    io:format("~n~nExecutando testes~n~n"),
    io:format("----------------------~n"),
    test_cicleway_speed_must_be_greater_than_ciclelane_speed(),
    test_ciclelane_speed_must_be_greater_than_mixed_traffic_speed(),
    test_descent_speed_must_be_greater_than_plane_speed(),
    test_plane_speed_must_be_greater_than_climb_speed(),
    test_speed_is_greater_for_less_occupation_when_cicleway(),
    test_speed_is_greater_for_less_occupation_when_mixed_traffic(),
    test_speed_is_1ms_for_saturated_link_when_cicleway(),
    test_speed_is_1ms_for_saturated_link_when_mixed_traffic(), 
    test_personal_speed_should_be_probabilistic(),
    io:format("~nTestes executados =)~n").



test_cicleway_speed_must_be_greater_than_ciclelane_speed() ->

    CaseTest = "Velocidade em ciclovia tem que ser maior que velocidade em ciclofaixa",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 0,
    NumberBikes = 15,
    IsCycleway = true,
    IsCyclelane = false,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedInCicleway = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedInCicleway),

    IsCycleway2 = false,
    IsCyclelane2 = true,

    SpeedInCiclelane = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway2, IsCyclelane2, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedInCiclelane),

    assertXGreaterThanY(SpeedInCicleway, SpeedInCiclelane),
    
    fimDoTestCase().




test_ciclelane_speed_must_be_greater_than_mixed_traffic_speed() ->

    CaseTest = "Velocidade em ciclofaixa tem que ser maior que velocidade em tráfego misto",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 0,
    NumberBikes = 15,
    IsCycleway = false,
    IsCyclelane = true,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedInCiclelane = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedInCiclelane),

    IsCycleway2 = false,
    IsCyclelane2 = false,

    SpeedInMixedTraffic = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway2, IsCyclelane2, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedInMixedTraffic),

    assertXGreaterThanY(SpeedInCiclelane, SpeedInMixedTraffic),
    
    fimDoTestCase().





test_descent_speed_must_be_greater_than_plane_speed() ->

    CaseTest = "Velocidade na descida tem que ser maior que velocidade no plano",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 0,
    NumberBikes = 15,
    IsCycleway = false,
    IsCyclelane = true,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 700,

    SpeedInDescent = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedInDescent),

    AltitudeNodeFrom2 = 760,
    AltitudeNodeTo2 = 760,

    SpeedInPlane = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom2, AltitudeNodeTo2),
    assertReasonableSpeed(SpeedInPlane),

    assertXGreaterThanY(SpeedInDescent, SpeedInPlane),
    
    fimDoTestCase().




test_plane_speed_must_be_greater_than_climb_speed() ->

    CaseTest = "Velocidade no plano tem que ser maior que velocidade na subida",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 0,
    NumberBikes = 15,
    IsCycleway = false,
    IsCyclelane = true,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedInPlane = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedInPlane),

    AltitudeNodeFrom2 = 760,
    AltitudeNodeTo2 = 780,

    SpeedInClimb = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom2, AltitudeNodeTo2),
    assertReasonableSpeed(SpeedInClimb),

    assertXGreaterThanY(SpeedInPlane, SpeedInClimb),
    
    fimDoTestCase().










test_speed_is_greater_for_less_occupation_when_cicleway() ->

    CaseTest = "Velocidade tem que ser menor para vias mais ocupadas, quando via é ciclovia",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 2,
    NumberBikes = 5,
    IsCycleway = true,
    IsCyclelane = false,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedWithLessOccupation = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedWithLessOccupation),

    NumberCars2 = 30,
    NumberBikes2 = 40,

    SpeedWithMoreOccupation = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars2, NumberBikes2, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedWithMoreOccupation),

    assertXGreaterThanY(SpeedWithLessOccupation, SpeedWithMoreOccupation),
    
    fimDoTestCase().


test_speed_is_greater_for_less_occupation_when_mixed_traffic() ->

    CaseTest = "Velocidade tem que ser menor para vias mais ocupadas, quando via é com tráfego misto",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 2,
    NumberBikes = 5,
    IsCycleway = false,
    IsCyclelane = false,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedWithLessOccupation = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedWithLessOccupation),

    NumberCars2 = 30,
    NumberBikes2 = 40,

    SpeedWithMoreOccupation = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars2, NumberBikes2, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertReasonableSpeed(SpeedWithMoreOccupation),

    assertXGreaterThanY(SpeedWithLessOccupation, SpeedWithMoreOccupation),
    
    fimDoTestCase().




test_speed_is_1ms_for_saturated_link_when_cicleway() ->

    CaseTest = "Velocidade tem que ser 1m/s em via saturada, quando via é ciclovia",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 0,
    NumberBikes = 501,
    IsCycleway = true,
    IsCyclelane = false,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedInSaturatedLink = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertEquals(SpeedInSaturatedLink, 1),
    
    fimDoTestCase().








test_speed_is_1ms_for_saturated_link_when_mixed_traffic() ->

    CaseTest = "Velocidade tem que ser maior que 1m/s em via saturada, quando via é em tráfego misto",
    io:format("Executando teste ~p~n", [CaseTest]),

    PersonalSpeed = 12/3.6,
    Length = 10,
    Capacity = 100,
    NumberCars = 90,
    NumberBikes = 51,
    IsCycleway = false,
    IsCyclelane = false,
    AltitudeNodeFrom = 760,
    AltitudeNodeTo = 760,

    SpeedInSaturatedLink = get_speed_bike(PersonalSpeed, Length, Capacity, NumberCars, NumberBikes, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo),
    assertEquals(SpeedInSaturatedLink, 1),
    
    fimDoTestCase().











test_personal_speed_should_be_probabilistic() ->

    CaseTest = "Personal speed should be probabilistic",
    io:format("Executando teste ~p~n", [CaseTest]),

    Speed1 = get_personal_bike_speed(),
    assertReasonableSpeed(Speed1),
    Speed2 = get_personal_bike_speed(),
    assertReasonableSpeed(Speed2),
    Speed3 = get_personal_bike_speed(),
    assertReasonableSpeed(Speed3),

    assertNotEquals(Speed1, Speed2),
    assertNotEquals(Speed1, Speed3),
    assertNotEquals(Speed2, Speed3),
    
    fimDoTestCase().

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Daqui pra frente, arcabouço de testes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fimDoTestCase() ->
    io:format("--------------~n").

assertReasonableSpeed(VelocidadeBike) ->
    if
        (VelocidadeBike < 1/3.6) or (VelocidadeBike > 30/3.6) ->
            io:format("TESTE FALHOU!!! =( ~nVelocidade devia ser entre 1km/h e 30km/h, mas foi ~wkm/h~n", [VelocidadeBike*3.6]);
        true ->
            naoImporta
    end.

assertEquals(Value, Expected) ->
    if
        Value /= Expected ->
            io:format("TESTE FALHOU!!! =( ~nValor deveria ser ~w, mas foi ~w~n", [Expected, Value]);
        true ->
            io:format("") % não importa
    end.

assertXGreaterThanY(X, Y) ->
    if
        X =< Y ->
            io:format("TESTE FALHOU!!! =( ~nValor deveria ser maior que ~w, mas foi ~w~n", [Y, X]);
        true ->
            io:format("") % não importa
    end.

assertNotEquals(Value1, Value2) ->
    if
        Value1 == Value2 ->
            io:format("TESTE FALHOU!!! =( ~nValores deveriam ser diferentes, mas foral iguais: ~w~n", [Value1]);
        true ->
            io:format("") % não importa
    end.

