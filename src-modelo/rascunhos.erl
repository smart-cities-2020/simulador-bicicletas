% Velocidade para carro (do jeito q tá no simulador):
link_density_speed(Id, Length, Capacity, NumberCars, Freespeed, _Lanes) ->

	Alpha = 1,
	Beta = 1,
	Speed = case NumberCars >= Capacity of
		true -> 1.0;
		false -> Freespeed * math:pow(1 - math:pow((NumberCars / Capacity), Beta), Alpha)
	end,

	Time = (Length / Speed) + 1,
	{Id, round(Time), round(Length)}.

% TODO para carro: diminuir velocidade se tiver ciclofaixa>
    % se ciclofaixa Freespeed = Freespeed/X... X???
    % procurar na literatura
% TODO para carro: diminuir velocidade conforma a quantidade de bicicletas circulando no link
%       provavelmente função terá que receber também NumberBikes





% Para bike seria also assim...

% Id: Id do link (acho); não é usado na função, mas é retornado
% Length: comprimento da via (em metros?)
% Capacity: quantos carros o link suporta
% NumberBikes: quantas bikes estão no link naquele instante
% NumberCars: quantos carros estão no link naquele instante
% CarFreespeed: velocidade do carro sem trânsito nenhum (nos afeta?)
% IsCycleway: boolean
% IsCyclelane: boolean
% AltitudeNodeFrom: altitude do nó de origem (na mesma unidade de Length)
% AltitudeNodeTo: altitude do nó de destino (na mesma unidade de Length)
% HourOfTheDay: um número entre 0 e 23
% _Lanes: ??? parece q não é usado
get_speed_bike(Id, Length, Capacity, NumberCars, NumberBikes, CarFreespeed, IsCycleway, IsCyclelane, AltitudeNodeFrom, AltitudeNodeTo, HourOfTheDay, _Lanes) ->

    % Primeiro considera tipo de infraestrutura cicloviária e inclinação

    IsMixedTraffic = (not IsCycleway) and (not IsCyclelane)
    Inclination = (AltitudeNodeTo -AltitudeNodeFrom) / Length
    Climb = Inclination > 0.02
    Descent = Inclination < -0.02
    Plane = (not Climb) and (not Descent)

    if

        IsCyclelane and Plane ->
            Freespeed = 9.6/3.6 % medido
        IsCyclelane and Climb ->
            Freespeed = 6.8/3.6 % medido, equivale a plano/1,41
        IsCyclelane and Descent->
            Freespeed = 15.1/3.6 % medidao, equivale a plano*1,57

        % Obs: Se tiver marcado ciclofaixa e ciclovia, considera ciclovia
        IsCycleway and Plane ->
            Freespeed = 15.5/3.6 % medido
        IsCycleway and Climb ->
            Freespeed = (15.5/3.6) / 1.41 % aplica mesmo fator encontrado pra ciclofaixa
        IsCycleway and Descent->
            Freespeed = (15.5/3.6) * 1.57 % aplica mesmo fator encontrado pra ciclofaixa

        IsMixedTraffic and Plane ->
            Freespeed = ...
        IsMixedTraffic and Climb ->
            Freespeed = ...
        IsMixedTraffic and Descent->
            Freespeed = ...

        true ->
            erlang:error("It should never happen, unexpected combination of arguments when calculating bike speed: IsCycleway=" ++ IsCycleway ++ ", IsCyclelane=" ++ IsCyclelane ++ ", AltitudeNodeFrom=" ++ AltitudeNodeFrom ++ ", AltitudeNodeTo=" ++ AltitudeNodeTo)
    end,

    % Segundo considera horário do dia (de manhã, indo pro trabalho, os ciclistas são mais rápidos)
    AvegereHourSpeed = avegereHourSpeedFor(Freespeed, HourOfTheDay),

    % Terceiro considera dispersão probabilística (há ciclistas mais rápidos e outros mais lentos)
    Mean = AvegereHourSpeed,
    StandardDeviation = ...,
    ProbabilisticSpeed = normalSample(Mean, StandardDeviation), % ou alguma outra curva

    % Quarto considera o tráfego
    if
        IsMixedTraffic ->
            % cellsize do carro = 7,5 e cellsize da bike = 3
            % cellsize é o comprimento do veículo mais a distância de segurança
            % Considera também que em uma faixa de carro passam duas bikes uma do lado da outra 
            Occupation = NumbersBike/(2.5*2) + NumberCars % precisa de cast pra float?
        true -> % ciclovia ou ciclofaixa (é uma bike atrás da outra)
            Occupation = NumbersBike/(2.5*1)
    end,

    % Se tráfego misto: o que importa é a curva, sem o "true->1"
    % Se ciclovia/faixa: mesma lógico do carro (se lotada: 1; se não: curva)  

	Alpha = 1,
	Beta = 1,
	Speed = case Occupation >= Capacity of
		true -> 1.0;
		false -> ProbabilisticSpeed * math:pow(1 - math:pow((Occupation / Capacity), Beta), Alpha)
	end,

    

	Time = (Length / Speed) + 1,
	{Id, round(Time), round(Length)}.


% ~~~~~~~~~~~~~~~~~~
% E-mail do Higor sobre velocidades calculei as velocidades médias de acordo com os critérios que conversamos ontem. Para as médias em ciclovia ou ciclofaixa planas, não há grandes diferenças entre a soma de todas as viagens (ida e volta) e as velocidades médias considerando ida e volta separadamente. Já para as viagens em ciclofaixa com inclinação, a diferença entre ida e volta é grande, tanto na velocidade quanto no número de viagens realizadas (as viagens no sentido de subida são pouco de 10% do total).
% 
% 1. Ciclofaixa plana: metrô faria lima (1) - r dos pinheiros (220), ciclofaixa da R. dos Pinheiros
% - ida + volta: 9,61 km/h
% - 1 -> 220: 9,19 km/h
% - 220 -> 1: 10,08 km/h
% 
% 2. Ciclofaixa c/ inclinação: itaú cultural (100) - ginásio do ibirapuera (73), ciclofaixas das ruas R Manoel da Nobrega e R Dr. Rafael de Barros
% - ida + volta: 13,38 km/h
% - 100 -> 73: 15,07 km/h (descida)
% - 73 -> 100: 6,8 km/h (subida)
% 
% 3. Ciclovia plana: metrô faria lima (1) - R. Min. Jesuíno Cardoso (27): Ciclovia da Faria Lima
% - ida + volta: 15,52 km/h
% - 1 -> 27: 16,2 km/h
% - 27 -> 1: 14,97 km/h



% Ideia de testes para get_speed_bike
% É meio difícil fazer testes com valores exatos esperados para get_speed_bike
% Um ou outro poderia ser útil como teste de regressão (mas aí é preciso controlar o fator probabilístico)
% Mas algo que seria mais interessante é o teste fazer asserções do tipo:
% Ciclovia é mais rápido que ciclofaixa que é mais rápido que tráfego misto
% Descida é mais rápido que plano, que é mais rápido que subida
% Começo da manhã é mais rápido que fim da tarde
% Muita ocupação da rua deixa mais lento que pouca ocupação
% E sempre verificando um range de razoabilidade (ex: entre 1 e 30km/h)





