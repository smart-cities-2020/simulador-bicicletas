diff --git a/src/class_Bike.erl b/src/class_Bike.erl
deleted file mode 100644
index 3872a9a..0000000
--- a/src/class_Bike.erl
+++ /dev/null
@@ -1,234 +0,0 @@
--module(class_Bike).
-% based on class_Car
-
-% "Teste" de class_Bike é a compilação.
-%
-% Método 1
-% Primeiro roda "erlc class_Bike.erl" pra antecipar alguns erros de sintaxe.
-% Aí roda "docker build -t interscsimulator .", que vai tentar compilar class_Bike
-% e dar erro se não conseguir.
-%
-% Método 2
-% Instalar o Erlang 20 (https://github.com/kerl/kerl).
-% Aí roda "make all" na raiz do zip só uma vez, para compilar o sim-diasca.
-% Depois roda "make" em zip/mock-simulators/smart_city_model/src,
-% isso vai compilar todas as classes em src, inclusive class_bike.
-% Esse método dá mais trabalho pra configurar, mas a compilação fica instântanea.
-% No método 1 demora bastante pra ter feedback.
-
-% remover
-% DigitalRailsCapable
-
--define( wooper_superclasses, [ class_Actor ] ).
-
-% parameters taken by the constructor ('construct').
-% Trips: list of trips since the trip can be a multi-trip
-%        each trip is a tuple { Mode , Path , LinkOrigin }
-%        LinkOrigin is not used here; Mode is also not used, since here we assume mode is always bike 
-% Type: reason for the trip; not used
-% Mode: bike
-% Park and DigitalRailsCapable: not used (it seems it's hard to remove them from the constructor without ofending the compiler)
--define( wooper_construct_parameters, ActorSettings, BikeName , Trips , StartTime , Type , _Park , Mode, _DigitalRailsCapable ).
-
-% Declaring all variations of WOOPER-defined standard life-cycle operations:
-% (template pasted, just two replacements performed to update arities)
--define( wooper_construct_export, new/8, new_link/8,
-		 synchronous_new/8, synchronous_new_link/8,
-		 synchronous_timed_new/8, synchronous_timed_new_link/8,
-		 remote_new/9, remote_new_link/9, remote_synchronous_new/9,
-		 remote_synchronous_new_link/9, remote_synchronisable_new_link/9,
-		 remote_synchronous_timed_new/9, remote_synchronous_timed_new_link/9,
-		 construct/9, destruct/1 ).
-
-% Method declarations.
--define( wooper_method_export, actSpontaneous/1, onFirstDiasca/2, receive_signal_state/3 ).
-
-% Allows to define WOOPER base variables and methods for that class:
--include("smart_city_test_types.hrl").
-
-% Allows to define WOOPER base variables and methods for that class:
--include("wooper.hrl").
-
-% Creates a new agent that is a person that moves around the city
--spec construct( wooper:state(), class_Actor:actor_settings(),
-				class_Actor:name(), pid() , parameter() , parameter() , parameter() , parameter(), parameter() ) -> wooper:state().
-construct( State, ?wooper_construct_parameters ) ->
-
-	ActorState = class_Actor:construct( State, ActorSettings, BikeName ),
-
-	InitialTrip = lists:nth( 1 , Trips ),	
-	Path = element( 2 , InitialTrip ),
-
-	case Mode of 
-		bike -> ok;
-		_ ->
-			erlang:error("It should never happen; class_Bike should receive only bike as mode, but it received [" ++ Mode ++ "].")
-	end,
-
-	% each bike agent has a different personal speed
-	PersonalSpeed = traffic_models:get_personal_bike_speed(),
-
-	InitialState = setAttributes( ActorState, [
-		{ bike_name, BikeName },
-		{ trips , Trips },
-		{ type, Type },
-		{ distance , 0 },
-		{ bike_position, -1 },
-		{ start_time , StartTime },
-		{ path , Path },
-		{ mode , Mode },
-		{ last_vertex , ok },
-		{ last_vertex_pid , ok },
-		{ previous_dr_name, nil },
-        { personal_speed, PersonalSpeed}]
-	),
-    InitialState.
-
-
--spec destruct( wooper:state() ) -> wooper:state().
-destruct( State ) ->
-	State.
-
--spec actSpontaneous( wooper:state() ) -> oneway_return().
-actSpontaneous( State ) ->	
-	Trips = getAttribute( State , trips ), 
-	Path = getAttribute( State , path ), 
-	verify_next_action( State , Trips , Path ).
-
-verify_next_action( State , _Trip , Path ) when Path == false ->
-	executeOneway( State , declareTermination );
-
-verify_next_action( State , Trips , Path ) when length( Trips ) == 0, Path == finish -> 
-	executeOneway( State , declareTermination );
-
-verify_next_action( State , Trips , Path ) when length( Trips ) > 0 ->
-	?wooper_return_state_only( request_position( State , Path ) );
-
-verify_next_action( State , _Trips , _Path ) ->
-	Type = getAttribute( State , type ),						
-	TotalLength = getAttribute( State , distance ),
-	StartTime = getAttribute( State , start_time ),
-	BikeId = getAttribute( State , bike_name ),	
-	LastPosition = getAttribute( State , bike_position ),
-	Mode = getAttribute( State , mode ), 
-
-	CurrentTickOffset = class_Actor:get_current_tick_offset( State ), 
-	print:write_final_message( Type , TotalLength , StartTime , BikeId , CurrentTickOffset , LastPosition , Mode , csv ),
-	PathFinish = setAttribute( State , path , finish ),
-
-	executeOneway( PathFinish , scheduleNextSpontaneousTick ).
-
-request_position( State , Path ) when Path == finish ->
-	CurrentTickOffset = class_Actor:get_current_tick_offset( State ),
-	Trips = getAttribute( State , trips ), 
-	NewTrips = list_utils:remove_element_at( Trips , 1 ),
-	
-	NewState = case length( NewTrips ) > 0 of
-		true -> 
-			InitialTrip = lists:nth( 1 , NewTrips ),	
-			NewPath = element( 2 , InitialTrip ),
-			setAttributes( State , [ { trips , NewTrips } , { path, NewPath} ] );
-		false -> 
-			setAttributes( State , [ { trips , NewTrips } , { path, ok} ] )
-	end,
-	executeOneway( NewState , addSpontaneousTick , CurrentTickOffset + 1 );	
-
-
-request_position( State , Path ) ->
-	case length( Path ) > 1 of
-		true ->	get_next_vertex( State , Path );
-		false -> 
-	        FinalState = setAttribute( State, path , finish ),
-	        executeOneway( FinalState , scheduleNextSpontaneousTick )
-	end.
-
-
-
-get_next_vertex( State, [ CurrentVertex | _ ] ) -> % Baseado no Mode != walk do class_car
-	LastVertex = getAttribute(State, last_vertex),
-
-	% Current vertex is an atom here, but at the ets it is a string. Must convert:
-	CurrentVertexStr = lists:flatten(io_lib:format("~s", [CurrentVertex])),
-	Matches = ets:lookup(traffic_signals, CurrentVertexStr),
-
-	case length(Matches) of
-		0 -> move_to_next_vertex(State);
-	 	_ -> 	
-			case LastVertex of
-				ok -> move_to_next_vertex(State);
-				_ ->
-					{_, TrafficSignalsPid} = lists:nth(1, Matches),
-					class_Actor:send_actor_message(TrafficSignalsPid, {querySignalState, LastVertex}, State)
-			end
-	 end.
-
-
-
-move_to_next_vertex( State ) ->
-	[ CurrentVertex | [ NextVertex | Path ] ] = getAttribute( State , path ),
-	Edge = list_to_atom(lists:concat([ CurrentVertex , NextVertex ])),
-
-	DecrementVertex = getAttribute( State , last_vertex_pid ),
-
-    LinkData = lists:nth(1, ets:lookup(list_streets , Edge)),
-    {_, Id, Length, Capacity, _Freespeed, Occupation, _Lanes, _DR, IsCycleway, IsCyclelane, Inclination} = LinkData,
-
-    Ocupation = if 
-        % Cellsize do carro = 7,5 e cellsize da bike = 3
-        % Cellsize é o comprimento do veículo mais a distância de segurança
-        % Considera também que em uma faixa de carro passam duas bikes uma do lado da outra 
-        % Na ciclovia ou ciclofaixa é uma bike atrás da outra
-        (IsCycleway or IsCyclelane) -> 
-            1/2.5;
-        true ->
-            1/5
-    end,
-		
-	case DecrementVertex of
-		ok -> ok;
-		_ -> ets:update_counter( list_streets, DecrementVertex , { 6 , -Ocupation })
-	end,	
-	ets:update_counter( list_streets , Edge , { 6 , Ocupation }),
-	
-    PersonalSpeed = getAttribute( State , personal_speed ),
-    NumberBikes = 1, % TODO obter NumberBikes
-    Speed = traffic_models:get_speed_bike(PersonalSpeed, Length, Capacity, Occupation, NumberBikes, IsCycleway, IsCyclelane, Inclination), 
-    Time = round((Length / Speed) + 1),
-    Distance = round(Length),
-
-	TotalLength = getAttribute( State , distance ) + Distance,
-	StateAfterMovement = setAttributes( State , [
-		{distance , TotalLength} , {bike_position , Id} , {last_vertex, CurrentVertex}, {last_vertex_pid , Edge} , {path , [NextVertex | Path]}] ), 
-
-	% io:format("t=~p: ~p; ~p->~p ~n", [class_Actor:get_current_tick_offset(State), getAttribute(State, bike_name), CurrentVertex, NextVertex]),
-	% io:format("~p Tick: ~p; ~p => ~p, Dist: ~p, Time: ~p, Avg. Speed: ~p, NextTick: ~p\n", 
-	% 	[getAttribute( State , bike_name ), class_Actor:get_current_tick_offset( State ), CurrentVertex, NextVertex, Distance, Time, Distance / Time, class_Actor:get_current_tick_offset( StateAfterMovement ) + Time]),
-
-	executeOneway( StateAfterMovement , addSpontaneousTick , class_Actor:get_current_tick_offset( StateAfterMovement ) + Time ).
-
-
-
-
-
--spec receive_signal_state(wooper:state(), tuple(), pid()) -> oneway_return().
-receive_signal_state( State , {Color, TicksUntilNextColor}, _TrafficLightPid ) -> 
-	case Color of
-		red -> 
-			% io:format("[~p] red (green in ~p)\n", [TrafficLightPid, TicksUntilNextColor]),
-			% Act spontaneously when the traffic light is green again...
-			executeOneway( State , addSpontaneousTick , class_Actor:get_current_tick_offset( State ) + TicksUntilNextColor );
-		green -> 
-			% io:format("Traffic signal is green, continuing movement...\n"),
-			move_to_next_vertex(State)
-	end.
-
- 
-
--spec onFirstDiasca( wooper:state(), pid() ) -> oneway_return().
-onFirstDiasca( State, _SendingActorPid ) ->
-	% TODO: Why this is needed?
-	StartTime = getAttribute( State , start_time ),
-    	FirstActionTime = class_Actor:get_current_tick_offset( State ) + StartTime,   	
-	NewState = setAttribute( State , start_time , FirstActionTime ),
-	executeOneway( NewState , addSpontaneousTick , FirstActionTime ).
-
diff --git a/src/class_CarManager.erl b/src/class_CarManager.erl
index c4d43f4..2366687 100644
--- a/src/class_CarManager.erl
+++ b/src/class_CarManager.erl
@@ -78,8 +78,6 @@ init_cars( [ Car | Cars ] , State ) ->
 			create_person_car( Count , State , CarName , ListTripsFinal , Type , Park , Mode, DigitalRailsCapable );
 		platoon ->
 			create_person_car( Count , State , CarName , ListTripsFinal , Type , Park , Mode, DigitalRailsCapable );
-		bike ->
-			create_person_bike( Count , State , CarName , ListTripsFinal , Type , Park , Mode, DigitalRailsCapable );
 		_ ->
 			create_person_public( Count , State , CarName , ListTripsFinal , Type , Mode, DigitalRailsCapable )
 	end,
@@ -111,20 +109,6 @@ create_person_public( Count , State , CarName , ListTripsFinal , Type , Mode, Di
 
 	create_person_public( Count - 1 , NewState , CarName , ListTripsFinal , Type , Mode, DigitalRailsCapable ).
 
-
-create_person_bike( 0 , State , _CarName , _ListTripsFinal , _Type , _Park , _Mode, _DigitalRailsCapable ) -> State;
-create_person_bike( Count , State , CarName , ListTripsFinal , Type , Park , Mode, DigitalRailsCapable ) ->
-	CarFinalName = io_lib:format( "~s_~B", [ CarName , Count ] ),
-	% StartTime = class_RandomManager:get_uniform_value( 1200 ),
-	% TODO: Should be > 0. Why?
-	StartTime = 1,
-
-	NewState = class_Actor:create_actor( class_Bike,
-		[ CarFinalName , ListTripsFinal , StartTime , Type , Park , Mode, DigitalRailsCapable ] , State ),
-
-	create_person_bike( Count - 1 , NewState , CarName , ListTripsFinal , Type , Park , Mode, DigitalRailsCapable ).
-
-
 -spec onFirstDiasca( wooper:state(), pid() ) -> oneway_return().
 onFirstDiasca( State, _SendingActorPid ) ->
     	FirstActionTime = class_Actor:get_current_tick_offset( State ) + 1,   	
diff --git a/src/class_Street.erl b/src/class_Street.erl
index f0af47a..5ddd3d9 100644
--- a/src/class_Street.erl
+++ b/src/class_Street.erl
@@ -102,7 +102,7 @@ iterate_list([]) -> ok;
 iterate_list([ Element | List ]) ->
 	
 	Vertices = element( 1, Element),
-	{ Id , Length , _ , Freespeed , Count, Lanes, {}, IsCycleway, IsCyclelane, Inclination } = element(2, Element),
+	{ Id , Length , _ , Freespeed , Count, Lanes, {} } = element(2, Element),
 
 	% CellSize = 7.5, % Cell size of 7.5m according to MATSim user guide
 	CellSize = 7.5,
@@ -113,16 +113,16 @@ iterate_list([ Element | List ]) ->
 	case Lanes == 1 of
 		true ->
 			StorageCapacity = math:ceil((Lanes) * Length / CellSize),
-			ets:insert(list_streets, {Vertices,  Id , Length , StorageCapacity , Freespeed , Count, Lanes, {}, IsCycleway, IsCyclelane, Inclination }),
+			ets:insert(list_streets, {Vertices,  Id , Length , StorageCapacity , Freespeed , Count, Lanes, {} }),
 	
 			StorageCapacityDR = math:ceil(1 * Length / CellSizeDR ),
-			ets:insert(list_streets_dr, {Vertices,  Id , Length , StorageCapacityDR , Freespeed , Count, Lanes, {}, IsCycleway, IsCyclelane, Inclination });
+			ets:insert(list_streets_dr, {Vertices,  Id , Length , StorageCapacityDR , Freespeed , Count, Lanes, {} });
 		false ->
 			StorageCapacity = math:ceil((Lanes - 1) * Length / CellSize),
-			ets:insert(list_streets, {Vertices,  Id , Length , StorageCapacity , Freespeed , Count, Lanes, {}, IsCycleway, IsCyclelane, Inclination }),
+			ets:insert(list_streets, {Vertices,  Id , Length , StorageCapacity , Freespeed , Count, Lanes, {} }),
 
 			StorageCapacityDR = math:ceil(1 * Length / CellSizeDR ),
-			ets:insert(list_streets_dr, {Vertices,  Id , Length , StorageCapacityDR , Freespeed , Count, Lanes, {}, IsCycleway, IsCyclelane, Inclination })
+			ets:insert(list_streets_dr, {Vertices,  Id , Length , StorageCapacityDR , Freespeed , Count, Lanes, {} })
 	end,
 
 	iterate_list( List ).
diff --git a/src/map_parser.erl b/src/map_parser.erl
index 9ac8bb2..cb3a6a1 100644
--- a/src/map_parser.erl
+++ b/src/map_parser.erl
@@ -67,18 +67,8 @@ extract_node(Node , Graph ) ->
         #xmlElement{name=Name, attributes=Attributes} ->
 	    case Name of
 			node -> 
-				Id = children( Attributes , id ),
-				Z = children( Attributes , z ),
-				DefaultSaoPauloAltitude = 760,
-				Altitude = case Z of 
-					ok -> DefaultSaoPauloAltitude;
-					_ -> case string:to_float(Z) of
-									{error,no_float} -> list_to_integer(Z);
-									{F,_Rest} -> F
-								end
-				end,
-				NodeData = { Altitude }, 
-				digraph:add_vertex(Graph, list_to_atom(Id), NodeData);	
+				Id = children( Attributes , id ),	
+				digraph:add_vertex(Graph, list_to_atom(Id));	
 			_ -> ok
 	    end;    
 		_ -> ok
@@ -92,19 +82,12 @@ extract_link(Link , Graph ) ->
 					Id = children( Attributes , id ),	
 					From = children( Attributes , from ),
 					To = children( Attributes , to ),
-					{_IdFrom, {AltitudeFrom}} = digraph:vertex(Graph, list_to_atom(From)),
-					{_IdTo, {AltitudeTo}} = digraph:vertex(Graph, list_to_atom(To)),
 					Length = children( Attributes , length ),
-					{LengthFloat, _} = string:to_float(Length),
-					Inclination = (AltitudeTo - AltitudeFrom) / LengthFloat,
 					Capacity = children ( Attributes , capacity ),
 					Freespeed = children( Attributes , freespeed ),
 					Lanes = children( Attributes , permlanes ),
-          IsCycleway = children( Attributes , cycleway ) == "true",
-          IsCyclelane = children( Attributes , cyclelane ) == "true",
 					RestrictedNextLinks = string:tokens(children(Attributes, restricted_next_links), ","),
-					LinkData = { Id , Length , Capacity , Freespeed, Lanes, RestrictedNextLinks, IsCycleway, IsCyclelane, Inclination },
-					digraph:add_edge(Graph, list_to_atom(From), list_to_atom(To), LinkData);
+					digraph:add_edge(Graph, list_to_atom(From), list_to_atom(To), { Id , Length , Capacity , Freespeed, Lanes, RestrictedNextLinks });
 				_ -> ok
 	    end;
 		_ -> ok
@@ -149,4 +132,4 @@ print_edges(Graph, [Element | MoreElements]) ->
 	io:format("vInicio: ~s~n", [ element( 2 , Edge ) ]),
 	io:format("vFim: ~s~n", [ element( 3 , Edge) ]),
 	io:format("id: ~s~n", [ element( 4 , Edge) ]),
-	print_edges( Graph , MoreElements ).
+	print_edges( Graph , MoreElements ).
\ No newline at end of file
diff --git a/src/map_parser_test.erl b/src/map_parser_test.erl
deleted file mode 100644
index 15e2e29..0000000
--- a/src/map_parser_test.erl
+++ /dev/null
@@ -1,19 +0,0 @@
--module(map_parser_test).
-
--export([show_tests/0]).
-
-
-%%%%%%%%%%%%%%%%%
-% TESTES %%%%%%%%
-%%%%%%%%%%%%%%%%%
-
-show_tests() ->
-    io:format("~n~nExecutando testes~n~n"),
-    io:format("----------------------~n"),
-    Infilename = "small-network.xml",
-    map_parser:show(Infilename, false),
-    io:format("~nTestes executados (se não deu pau, então tá bom) =)~n").
-
-
-
-
diff --git a/src/personal_speed_distribution_for_bikes.csv b/src/personal_speed_distribution_for_bikes.csv
deleted file mode 100644
index ac39093..0000000
--- a/src/personal_speed_distribution_for_bikes.csv
+++ /dev/null
@@ -1,400000 +0,0 @@
-2.08627708575885
-5.3886157026093
-6.53516833802423
... (ocultado)
-3.92255597833727
diff --git a/src/run_map_parser_tests.sh b/src/run_map_parser_tests.sh
deleted file mode 100755
index 3700c25..0000000
--- a/src/run_map_parser_tests.sh
+++ /dev/null
@@ -1 +0,0 @@
-erlc map_parser.erl && erlc map_parser_test.erl && erl -noshell -s map_parser_test show_tests -s init stop
diff --git a/src/run_traffic_models_tests.sh b/src/run_traffic_models_tests.sh
deleted file mode 100755
index 802164a..0000000
--- a/src/run_traffic_models_tests.sh
+++ /dev/null
@@ -1 +0,0 @@
-erlc traffic_models.erl && erlc traffic_models_test.erl && erl -noshell -s traffic_models_test get_speed_bike_tests -s init stop
diff --git a/src/small-network.xml b/src/small-network.xml
deleted file mode 100644
index 86b4d05..0000000
--- a/src/small-network.xml
+++ /dev/null
@@ -1,43 +0,0 @@
-<?xml version="1.0" encoding="UTF-8"?>
-<!DOCTYPE network SYSTEM "http://www.matsim.org/files/dtd/network_v2.dtd">
-<network>
-
-<!-- ====================================================================== -->
-
-	<nodes>
-		<node id="1001568643" x="-7347433.28816257" y="-2852981.6323715686" z="800">
-		</node>
-		<node id="1001568979" x="-7347522.284498743" y="-2853614.5465874914" z="800.1">
-		</node>
-		<node id="1036648427" x="-7330256.36170953" y="-2853959.906814551" >
-		</node>
-		<node id="95967345" x="-7347966.628430043" y="-2858279.0762352236" >
-		</node>
-	</nodes>
-
-<!-- ====================================================================== -->
-
-	<links capperiod="01:00:00" effectivecellsize="7.5" effectivelanewidth="3.75">
-		<link id="1" from="1001568643" to="1001568979" length="13.539593517363432" freespeed="4.166666666666667" capacity="600.0" permlanes="1.0" oneway="1" modes="car" cycleway="true" cyclelane="false">
-			<attributes>
-				<attribute name="origid" class="java.lang.String" >37134902</attribute>
-				<attribute name="type" class="java.lang.String" >residential</attribute>
-			</attributes>
-		</link>
-		<link id="1" from="1001568643" to="1001568979" length="13.539593517363432" freespeed="4.166666666666667" capacity="600.0" permlanes="1.0" oneway="1" modes="car" >
-			<attributes>
-				<attribute name="origid" class="java.lang.String" >37134902</attribute>
-				<attribute name="type" class="java.lang.String" >residential</attribute>
-			</attributes>
-		</link>
-		<link id="1" from="1036648427" to="95967345" length="13.539593517363432" freespeed="4.166666666666667" capacity="600.0" permlanes="1.0" oneway="1" modes="car" cyclelane="true">
-			<attributes>
-				<attribute name="origid" class="java.lang.String" >37134902</attribute>
-				<attribute name="type" class="java.lang.String" >residential</attribute>
-			</attributes>
-		</link>				
-	</links>
-
-<!-- ====================================================================== -->
-
-</network>
diff --git a/src/traffic_models.erl b/src/traffic_models.erl
index b3a890e..3ac03bc 100644
--- a/src/traffic_models.erl
+++ b/src/traffic_models.erl
@@ -1,141 +1,37 @@
 -module(traffic_models).
 
--export([get_speed_car/2, get_speed_walk/2, get_speed_bike/6, get_personal_bike_speed/0]).
+-export([get_speed_car/2, get_speed_walk/2]).
 
-% Occupation: the count of vehicles in the link; one unit corresponds to one car; 
-%             one bike counts 1/5 of occupation for mixed traffic and
-%                             1/2.5 for cicleways and cyclelanes.
-%             It is the "Count" of the link.
 % There is DR in link and car can use it:
-get_speed_car({_, Id, Length, RawCapacity, Freespeed, Occupation, Lanes, {_DRName, _DigitalRailsLanes, _Cycle, _Bandwidth, _Signalized, _Offset}}, true) ->
-	link_density_speed(Id, Length, RawCapacity, Occupation, Freespeed, Lanes);
+get_speed_car({_, Id, Length, RawCapacity, Freespeed, NumberCars, Lanes, {_DRName, _DigitalRailsLanes, _Cycle, _Bandwidth, _Signalized, _Offset}}, true) ->
+	link_density_speed(Id, Length, RawCapacity, NumberCars, Freespeed, Lanes);
 
 % There is DR but not effective:
-get_speed_car({Whatever, Id, Length, RawCapacity, Freespeed, Occupation, Lanes, {_DRName, _DigitalRailsLanes, _Cycle, _Bandwidth, _Signalized, _Offset}}, noeffect) ->
-	get_speed_car({Whatever, Id, Length, RawCapacity, Freespeed, Occupation, Lanes, {}}, noeffect);
+get_speed_car({Whatever, Id, Length, RawCapacity, Freespeed, NumberCars, Lanes, {_DRName, _DigitalRailsLanes, _Cycle, _Bandwidth, _Signalized, _Offset}}, noeffect) ->
+	get_speed_car({Whatever, Id, Length, RawCapacity, Freespeed, NumberCars, Lanes, {}}, noeffect);
 
 % There is DR but car cannot use it:
-get_speed_car({_, Id, Length, RawCapacity, Freespeed, Occupation, Lanes, {_DRName, _DigitalRailsLanes, _Cycle, _Bandwidth, _Signalized, _Offset}}, false) ->
-	link_density_speed(Id, Length, RawCapacity, Occupation, Freespeed, Lanes);
+get_speed_car({_, Id, Length, RawCapacity, Freespeed, NumberCars, Lanes, {_DRName, _DigitalRailsLanes, _Cycle, _Bandwidth, _Signalized, _Offset}}, false) ->
+	link_density_speed(Id, Length, RawCapacity, NumberCars, Freespeed, Lanes);
 
 % There is no DR:
-get_speed_car({_, Id, Length, RawCapacity, Freespeed, Occupation, Lanes, {}}, _) ->
-	link_density_speed(Id, Length, RawCapacity, Occupation, Freespeed, Lanes).
+get_speed_car({_, Id, Length, RawCapacity, Freespeed, NumberCars, Lanes, {}}, _) ->
+	link_density_speed(Id, Length, RawCapacity, NumberCars, Freespeed, Lanes).
 
-link_density_speed(Id, Length, Capacity, Occupation, Freespeed, _Lanes) ->
+link_density_speed(Id, Length, Capacity, NumberCars, Freespeed, _Lanes) ->
 
 	Alpha = 1,
 	Beta = 1,
-	Speed = case Occupation >= Capacity of
+	Speed = case NumberCars >= Capacity of
 		true -> 1.0;
-		false -> Freespeed * math:pow(1 - math:pow((Occupation / Capacity), Beta), Alpha)
+		false -> Freespeed * math:pow(1 - math:pow((NumberCars / Capacity), Beta), Alpha)
 	end,
 
 	Time = (Length / Speed) + 1,
 	{Id, round(Time), round(Length)}.
 
-
-
-
 get_speed_walk(LinkData, _) ->
 	{_, Id, Length, _, _, _} = LinkData,	
 	Time = ( Length / 2 ),
 
-	{Id, ceil(Time), round(Length)}.
-
-
-
-%%%%%%%%%%%%%%% BIKES %%%%%%%%%%%%%%%%%%%%%%%
-
-get_personal_bike_speed() ->
-    % The sampled speed is retrieved from a previously generated list following a distribution observed in the OD (Pesquisa OrigemDestino) data.
-    SampledSpeed = get_next_value_from_speeds_distribution(),
-    % What matters in this speeds list is the distribution (a generalized gama, as told us by the fit test done in R).
-    % But the speeds in our OD dataset are lower than actual speeds, because we used as distance the euclidean distance from origin to destination - a straight line linking origin and destination. 
-    % The mean speed for the OD dataset is 7.5km/h.
-    % We considered the Bike Sampa dataset to me more accurate. In this Bike Sampa dataset the mean speed mixed traffic is 10km/h.
-    % Therefore, we apply a factor over the sampled speed to correct the offset of the speed distribution.
-    FactorOdToBikeSampa = 10/7.44,
-    SampledSpeed * FactorOdToBikeSampa.
-
-get_next_value_from_speeds_distribution() ->
-
-    % First, we open the file if it's the first time this function is invoked
-    case lists:member(table_personal_speeds, ets:all()) of
-        false -> 
-            ets:new(table_personal_speeds, [named_table, protected, set, {keypos, 1}]),
-            Filename = "personal_speed_distribution_for_bikes.csv",
-            {_ok, File} = file:open(Filename, read),
-            ets:insert(table_personal_speeds, {file, File});
-            % we keep the file in the EST, so we keep implicitly together the pointer to the last read line.
-        _ -> nothing_to_do
-    end,
-
-    [{_, SpeedsFile}] = ets:lookup(table_personal_speeds, file),
-    Line = io:get_line(SpeedsFile, ''), % read next line of the file
-    % Obs: if we got at the end of file, the system will crash! But we hope this to not happen!
-    {SpeedKmh, _} = string:to_float(Line),
-    SpeedKmh/3.6.
-
-% PersonalSpeed: different people have different speeds; each agent must hold a personal speed generated once for the actor. The personal speed must be generated using the function get_personal_bike_speed.
-% Capacity: how many cars the link supports
-% Occupation: the count of vehicles in the link; one unit corresponds to one car; 
-%             one bike counts 1/5 of occupation for mixed traffic and
-%                             1/2.5 for cicleways and cyclelanes.
-% IsCycleway: boolean
-% IsCyclelane: boolean
-% Inclination: (altitude_to - altitude_from) / length 
-get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination) ->
-    Freespeed = get_free_speed_for_bike(PersonalSpeed, IsCycleway, IsCyclelane, Inclination),
-    Speed = speed_for_bike_considering_traffic(Freespeed, Capacity, Occupation),
-    Speed.
-
-
-
-get_free_speed_for_bike(PersonalSpeed, IsCycleway, IsCyclelane, Inclination) ->
-
-    Climb = Inclination > 0.02,
-    Descent = Inclination < -0.02,
-    IsMixedTraffic = (not IsCycleway) and (not IsCyclelane),
-    Plane = (not Climb) and (not Descent),
-
-    % The factors were defined based on analysis of real data from Bike Sampa
-    if 
-        Plane and IsCycleway ->
-            PersonalSpeed * 1.4651;
-        Plane and IsCyclelane ->
-            PersonalSpeed * 0.5524;
-        Plane and IsMixedTraffic ->
-            PersonalSpeed * 1.5;
-        Climb and (IsCycleway or IsCyclelane) ->
-            PersonalSpeed * 0.3827;
-        Climb and IsMixedTraffic ->
-            PersonalSpeed * 0.8027;
-        Descent and (IsCycleway or IsCyclelane) ->
-            PersonalSpeed * 1.4532;
-        Descent and IsMixedTraffic ->
-            PersonalSpeed * 1.1217;
-        true ->
-            erlang:error("It should never happen, unexpected combination of arguments when calculating bike speed: IsCycleway=" ++ IsCycleway ++ ", IsCyclelane=" ++ IsCyclelane ++ ", Inclination=" ++ Inclination)
-    end.
-   
-
-
-
-
-
-
-
-speed_for_bike_considering_traffic(BaseSpeed, Capacity, Occupation) ->
-
-    SaturatedLink = Occupation >= Capacity,
-
-	Alpha = 1,
-	Beta = 1,
-    if
-        SaturatedLink ->
-             1.0;
-        true -> % (não saturado) ou (saturado em tráfego misto)
-            BaseSpeed * math:pow(1 - math:pow((Occupation / Capacity), Beta), Alpha)
-	end.
-
+	{Id, ceil(Time), round(Length)}.
\ No newline at end of file
diff --git a/src/traffic_models_test.erl b/src/traffic_models_test.erl
deleted file mode 100644
index 03d55c4..0000000
--- a/src/traffic_models_test.erl
+++ /dev/null
@@ -1,333 +0,0 @@
--module(traffic_models_test).
-
--export([get_speed_bike_tests/0]).
-
-
-%%%%%%%%%%%%%%%%%
-% TESTES %%%%%%%%
-%%%%%%%%%%%%%%%%%
-% Para executar o teste:
-% erlc traffic_models.erl
-% erl -noshell -s traffic_models get_speed_bike_test -s init stop
-
-get_speed_bike_tests() ->
-    io:format("~n~nExecutando testes~n~n"),
-    io:format("----------------------~n"),
-    test_cicleway_speed_must_be_greater_than_ciclelane_speed(),
-    test_mixed_traffic_speed_must_be_greater_than_cycleway_speed(),
-    test_descent_speed_must_be_greater_than_plane_speed(),
-    test_plane_speed_must_be_greater_than_climb_speed(),
-    test_speed_is_greater_for_less_occupation_when_cicleway(),
-    test_speed_is_greater_for_less_occupation_when_mixed_traffic(),
-    test_speed_is_1ms_for_saturated_link_when_cicleway(),
-    test_speed_is_1ms_for_saturated_link_when_mixed_traffic(), 
-    test_personal_speed_should_be_probabilistic(),
-    io:format("~nTestes executados =)~n").
-
-
-
-test_cicleway_speed_must_be_greater_than_ciclelane_speed() ->
-
-    CaseTest = "Velocidade em ciclovia tem que ser maior que velocidade em ciclofaixa",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 0,
-    NumberBikes = 15,
-    IsCycleway = true,
-    IsCyclelane = false,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,
-
-    SpeedInCicleway = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedInCicleway),
-
-    IsCycleway2 = false,
-    IsCyclelane2 = true,
-    Occupation2 = occupation_for(NumberCars, NumberBikes, IsCycleway2, IsCyclelane2),
-
-    SpeedInCiclelane = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation2, IsCycleway2, IsCyclelane2, Inclination),
-    assertReasonableSpeed(SpeedInCiclelane),
-
-    assertXGreaterThanY(SpeedInCicleway, SpeedInCiclelane),
-    
-    fimDoTestCase().
-
-
-occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane) ->
-    if
-        IsCycleway or IsCyclelane ->
-            NumberBikes/2.5;
-        true ->
-            NumberCars + NumberBikes/5
-    end.
-
-
-test_mixed_traffic_speed_must_be_greater_than_cycleway_speed() ->
-
-    CaseTest = "Velocidade em tráfego misto tem que ser maior que velocidade em ciclovia (!!!)",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 10,
-    NumberBikes = 5,
-    IsCycleway = false,
-    IsCyclelane = false,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,    
-
-    SpeedInMixedTraffic = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedInMixedTraffic),
-
-    IsCycleway2 = true,
-    IsCyclelane2 = false,
-
-    SpeedInCycleway = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway2, IsCyclelane2, Inclination),
-    assertReasonableSpeed(SpeedInMixedTraffic),
-
-    assertXGreaterThanY(SpeedInMixedTraffic, SpeedInCycleway),
-    
-    fimDoTestCase().
-
-
-
-
-
-test_descent_speed_must_be_greater_than_plane_speed() ->
-
-    CaseTest = "Velocidade na descida tem que ser maior que velocidade no plano",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 0,
-    NumberBikes = 15,
-    IsCycleway = false,
-    IsCyclelane = true,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = (700 - 760)/10,
-
-    SpeedInDescent = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedInDescent),
-
-    Inclination2 = 0,
-
-    SpeedInPlane = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination2),
-    assertReasonableSpeed(SpeedInPlane),
-
-    assertXGreaterThanY(SpeedInDescent, SpeedInPlane),
-    
-    fimDoTestCase().
-
-
-
-
-test_plane_speed_must_be_greater_than_climb_speed() ->
-
-    CaseTest = "Velocidade no plano tem que ser maior que velocidade na subida",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 0,
-    NumberBikes = 15,
-    IsCycleway = false,
-    IsCyclelane = true,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,
-
-    SpeedInPlane = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedInPlane),
-
-    Inclination2 = (780 - 760)/10,
-
-    SpeedInClimb = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination2),
-    assertReasonableSpeed(SpeedInClimb),
-
-    assertXGreaterThanY(SpeedInPlane, SpeedInClimb),
-    
-    fimDoTestCase().
-
-
-
-
-
-
-
-
-
-
-test_speed_is_greater_for_less_occupation_when_cicleway() ->
-
-    CaseTest = "Velocidade tem que ser menor para vias mais ocupadas, quando via é ciclovia",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 2,
-    NumberBikes = 5,
-    IsCycleway = true,
-    IsCyclelane = false,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,
-
-    SpeedWithLessOccupation = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedWithLessOccupation),
-
-    NumberCars2 = 30,
-    NumberBikes2 = 40,
-    Occupation2 = occupation_for(NumberCars2, NumberBikes2, IsCycleway, IsCyclelane),
-
-    SpeedWithMoreOccupation = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation2, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedWithMoreOccupation),
-
-    assertXGreaterThanY(SpeedWithLessOccupation, SpeedWithMoreOccupation),
-    
-    fimDoTestCase().
-
-
-test_speed_is_greater_for_less_occupation_when_mixed_traffic() ->
-
-    CaseTest = "Velocidade tem que ser menor para vias mais ocupadas, quando via é com tráfego misto",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 2,
-    NumberBikes = 5,
-    IsCycleway = false,
-    IsCyclelane = false,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,
-
-    SpeedWithLessOccupation = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedWithLessOccupation),
-
-    NumberCars2 = 30,
-    NumberBikes2 = 40,
-    Occupation2 = occupation_for(NumberCars2, NumberBikes2, IsCycleway, IsCyclelane),
-
-    SpeedWithMoreOccupation = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation2, IsCycleway, IsCyclelane, Inclination),
-    assertReasonableSpeed(SpeedWithMoreOccupation),
-
-    assertXGreaterThanY(SpeedWithLessOccupation, SpeedWithMoreOccupation),
-    
-    fimDoTestCase().
-
-
-
-
-test_speed_is_1ms_for_saturated_link_when_cicleway() ->
-
-    CaseTest = "Velocidade tem que ser 1m/s em via saturada, quando via é ciclovia",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 0,
-    NumberBikes = 501,
-    IsCycleway = true,
-    IsCyclelane = false,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,
-
-    SpeedInSaturatedLink = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertEquals(SpeedInSaturatedLink, 1),
-    
-    fimDoTestCase().
-
-
-
-
-
-
-
-
-test_speed_is_1ms_for_saturated_link_when_mixed_traffic() ->
-
-    CaseTest = "Velocidade tem que ser 1m/s em via saturada, quando via é em tráfego misto",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    PersonalSpeed = 12/3.6,
-    Capacity = 100,
-    NumberCars = 90,
-    NumberBikes = 51,
-    IsCycleway = false,
-    IsCyclelane = false,
-    Occupation = occupation_for(NumberCars, NumberBikes, IsCycleway, IsCyclelane),
-    Inclination = 0,
-
-    SpeedInSaturatedLink = traffic_models:get_speed_bike(PersonalSpeed, Capacity, Occupation, IsCycleway, IsCyclelane, Inclination),
-    assertEquals(SpeedInSaturatedLink, 1),
-    
-    fimDoTestCase().
-
-
-
-
-
-
-
-
-
-
-
-test_personal_speed_should_be_probabilistic() ->
-
-    CaseTest = "Personal speed should be probabilistic",
-    io:format("Executando teste ~p~n", [CaseTest]),
-
-    Speed1 = traffic_models:get_personal_bike_speed(),
-    assertReasonableSpeed(Speed1),
-    Speed2 = traffic_models:get_personal_bike_speed(),
-    assertReasonableSpeed(Speed2),
-    Speed3 = traffic_models:get_personal_bike_speed(),
-    assertReasonableSpeed(Speed3),
-
-    assertNotEquals(Speed1, Speed2),
-    assertNotEquals(Speed1, Speed3),
-    assertNotEquals(Speed2, Speed3),
-    
-    fimDoTestCase().
-
-%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-% Daqui pra frente, arcabouço de testes
-%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-
-fimDoTestCase() ->
-    io:format("--------------~n").
-
-assertReasonableSpeed(VelocidadeBike) ->
-    if
-        (VelocidadeBike < 1/3.6) or (VelocidadeBike > 30/3.6) ->
-            io:format("TESTE FALHOU!!! =( ~nVelocidade devia ser entre 1km/h e 30km/h, mas foi ~wkm/h~n", [VelocidadeBike*3.6]);
-        true ->
-            naoImporta
-    end.
-
-assertEquals(Value, Expected) ->
-    if
-        Value /= Expected ->
-            io:format("TESTE FALHOU!!! =( ~nValor deveria ser ~w, mas foi ~w~n", [Expected, Value]);
-        true ->
-            io:format("") % não importa
-    end.
-
-assertXGreaterThanY(X, Y) ->
-    if
-        X =< Y ->
-            io:format("TESTE FALHOU!!! =( ~nValor deveria ser maior que ~w, mas foi ~w~n", [Y, X]);
-        true ->
-            io:format("") % não importa
-    end.
-
-assertNotEquals(Value1, Value2) ->
-    if
-        Value1 == Value2 ->
-            io:format("TESTE FALHOU!!! =( ~nValores deveriam ser diferentes, mas foral iguais: ~w~n", [Value1]);
-        true ->
-            io:format("") % não importa
-    end.
-
