# MAC6922 - Tópicos Avançados de Pesquisa em Cidades Inteligentes

## Script para adicionar altitudes aos links do simulador
# - Percorre XML network-input.xml (2-import-infraestrutura-cicloviaria-from-geosampa) e preenche os links com suas altitudes (alts_map_eduardo.csv) em network.xml

### Lendo acidentes e removendo aqueles fora do mapa de São Paulo

import geopandas as gpd
import pandas as pd
from xml.dom.minidom import parse, parseString
import xml.etree.ElementTree as ET

# Reading altitudes
alt = pd.read_csv('alts_map_eduardo.csv', delimiter=',')
# Removing duplicated possible links
assert len(alt) == len(alt.id.unique())
print(alt.head())

### Associando altitude aos links

def write_xml(alt, pathway_in, pathway_out):
    # Openning XML file with São Paulo's nodes and links
    tree = ET.parse(pathway_in)
    root = tree.getroot()
    
    count = 0
    
    # Set altitudes as XML attribute 'alt'
    for elem in root.find('nodes').findall('node'):
        node_id = int(elem.attrib['id'])
        node_alt = float(alt[alt.id == node_id].alt)
        if bool(node_alt):
            elem.set('z', str(node_alt))
            #print('Link: ', node_id, ' with altitude = ', node_alt)
        else:
            # It returns an error if the node altitude is not in the csv
            elem.set('z', str(0))
            print('Link: ', node_id, ' NOT found in alts_map_eduardo.csv')
            count = count+1
    
    print('Percentage of altitudes not found: ', count/len(root.find('nodes').findall('node')))
    tree.write(pathway_out)

# Prettify
# Função que transforma o XML de uma linha só (que o Gedit não consegue abrir) para um XML indentado 
def prettify(pathway):
    dom1 = parse(pathway)
    text_file = open(pathway, 'w')
    text_file.write(dom1.toprettyxml(indent=' '*4))
    text_file.close()
    
print('Iniciando processamento dos links')

# Writing altitudes to XML links
pathway_in = 'network-input.xml'
pathway_out = 'network.xml'

write_xml(alt, pathway_in, pathway_out)
print('Altitudes inputadas em network.xml')
prettify(pathway_out)
print('network.xml prettified')
