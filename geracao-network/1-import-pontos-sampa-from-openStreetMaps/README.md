# Importing roads from OpenStreetMap

## Running the notebooks
1. Install the required Python3 packages listed on `requirements.txt`:
```sh
pip install -r requirements.txt
```
2. Run jupyter-notebook:
```sh
jupyter-notebook .
```
3. Open the desired notebook

## Troubleshooting
- If data files are not found, check if they are in /data before running. All of the dataset links are referenced as comments in the code.
