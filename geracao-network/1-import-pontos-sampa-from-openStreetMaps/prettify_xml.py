#!/bin/python3

import sys
from xml.dom.minidom import parse, parseString

def main():
    if (len(sys.argv) < 3):
        print("usage: prettify_xml.py <input_file_name> <output_file_name>")
        return 1
    dom1 = parse(sys.argv[1])
    text_file = open(sys.argv[2], 'w')
    text_file.write(dom1.toprettyxml(indent=' '*4))
    text_file.close()
    return 0

if __name__== "__main__":
    main()
