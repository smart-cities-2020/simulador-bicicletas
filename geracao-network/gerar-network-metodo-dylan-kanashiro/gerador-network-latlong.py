from xml.dom import minidom
import numpy as np
import geopy.distance


MAP_PATH = "edges_overpass_query.xml"
dom = minidom.parse(MAP_PATH)

def chunk_nodes(nodes):
    """
    Usage:
    > chunk_nodes([1,2,3,4,5])
    [(1, 2), (2, 3), (3, 4), (4, 5)]
    """
    list1 = list(nodes)[:-1]
    list2 = list(nodes)[1:]
    
    return list(zip(list1, list2))



ways = dom.getElementsByTagName('way')
nodes_tags = dom.getElementsByTagName('node')
nodes = {}
edges = {}

def calculate_distance(node1, node2):
    return geopy.distance.vincenty(nodes[node1], nodes[node2]).m



for n in nodes_tags:
    nodes[int(n.getAttribute('id'))] = (float(n.getAttribute('lat')), float(n.getAttribute('lon')))

highest = 0
counter = 1
for w in ways:
    tags = w.getElementsByTagName('tag')
    width = 2
    maxspeed = 0
    oneway = 1
    
    for t in tags:
        key = t.getAttribute('k')
        value = t.getAttribute('v')
        
        if (key == 'width'):
            width = float(value)
        elif (key == 'maxspeed'):
            maxspeed = int(value)/3.6
        elif (key == 'oneway'):
            oneway = 0
    
    chunks = chunk_nodes(w.getElementsByTagName('nd'))
    for chunk in chunks:
        n1 = chunk[0].getAttribute('ref')
        n2 = chunk[1].getAttribute('ref')
        
        length = calculate_distance(int(n1), int(n2))
        edge = {
            'freespeed': maxspeed,
            'length': length,
            'capacity': width*length,
            'from': int(n1),
            'to': int(n2),
            'oneway': oneway,
            'modes': 'car',
            'origid': int(w.getAttribute('id')),
            'type': 'residential'
        }
        edges[counter] = edge
        
        edge = {
            'freespeed': maxspeed,
            'length': length,
            'capacity': width*length,
            'from': int(n2),
            'to': int(n1),
            'oneway': oneway,
            'modes': 'car',
            'origid': int(w.getAttribute('id')),
            'type': 'residential'
        }
        counter += 1
        edges[counter] = edge
        counter += 1

import lxml.etree
import lxml.builder
from lxml import etree as ET

root_et = ET.Element('network')
nodes_et = ET.SubElement(root_et, 'nodes')
links_et = ET.SubElement(root_et, 'links')

for node_id, attrs in nodes.items():
    ET.SubElement(nodes_et, 'node', id=str(node_id), x=str(attrs[0]), y=str(attrs[1]))


for link_id, attrs in edges.items():
    ele = ET.SubElement(links_et, 'link',
                    id=str(link_id),
                  capacity=str(attrs["capacity"]),
                  freespeed=str(attrs["freespeed"]),
                  to=str(attrs["to"]),
                  length=str(attrs["length"]),
                  modes=str(attrs["modes"]),
                  oneway=str(attrs["oneway"]),
                    origid=str(attrs["origid"]),
                type=str(attrs["type"]))
    ele.set('from', str(attrs["from"]))
    
  
tree = ET.ElementTree(root_et)
map_path = '../scenario_2_sao_paulo/inputs/map.xml'
tree.write(map_path, pretty_print=True, xml_declaration=True,   encoding="utf-8")

len(edges)

