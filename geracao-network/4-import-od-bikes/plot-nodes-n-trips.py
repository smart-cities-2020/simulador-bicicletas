#!/bin/python3

#import tkinter
#import numpy as np
import tkinter as Tk
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
#import tkinter as tk
import pyproj

simulator_nodes = {} # id => (x, y)
xs_sim = []
ys_sim = []
xs_map = []
ys_map = []
xs_trip = []
ys_trip = []


def import_simulator_nodes(etree):
    root = etree.getroot()
    for node_xml in root.find('nodes').findall('node'):
        global simulator_nodes
        global xs_sim
        global ys_sim

        # Convertendo das coordenadas do simulador para lat,long
        #proj="+init=epsg:32719" # projeção do simulador 
        #proj="+init=epsg:4326" # projeção com lat long
        #this.gdf = gdf.to_crs(proj) # converte sistema de projeção
    
        node_id = node_xml.attrib['id']
        x = float(node_xml.attrib['x'])
        y = float(node_xml.attrib['y'])
        # Gambira para rodar com mapa que tem coordenadas invertidas
        #y = float(node_xml.attrib['x'])
        #x = float(node_xml.attrib['y'])
        #lat, longg = pyproj.transform('EPSG:32719', 'wgs84', x, y) # se fosse converter pra lat, long

        simulator_nodes[node_id] = (x, y)
        xs_sim.append(x)
        ys_sim.append(y)


    max_x = max(xs_sim)
    min_x = min(xs_sim)
    max_y = max(ys_sim)
    min_y = min(ys_sim)

    # PS.: a estratégia abixo não funcionou direito.
    #Usado quando o mapa do Eduardo estava em UTC (map-sp2.xml)
    #max_lat, max_long = pyproj.transform('EPSG:32719', 'wgs84', max_x, max_y) # se fosse converter pra lat, long
    #min_lat, min_long = pyproj.transform('EPSG:32719', 'wgs84', min_x, min_y) # se fosse converter pra lat, long

    max_long = max_x
    min_long = min_x
    max_lat = max_y
    min_lat = min_y

    print('max long: ', max_long)
    print('min long: ', min_long)
    print('max lat: ', max_lat)
    print('min lat: ', min_lat)

def import_map_nodes(etree):
    root = etree.getroot()
    for node_xml in root.find('nodes').findall('node'):
        global simulator_nodes
        global xs_map
        global ys_map

        node_id = node_xml.attrib['id']
        #x = float(node_xml.attrib['x'])
        #y = float(node_xml.attrib['y'])
        # Gambira para rodar com mapa que tem coordenadas invertidas
        y = float(node_xml.attrib['x'])
        x = float(node_xml.attrib['y'])

        simulator_nodes[node_id] = (x, y)
        xs_map.append(x)
        ys_map.append(y)

def import_links_do_simulador(etree):
    root = etree.getroot()
    # Se for um network.xml, buscar pela tag links primeiro.
    for link_xml in root.find('links').findall('link'):
        global xs_trip
        global ys_trip

        node_id_from = link_xml.attrib['from']
        node_id_to = link_xml.attrib['to']
        x_from, y_from = simulator_nodes[node_id_from]
        x_to, y_to = simulator_nodes[node_id_to]

        xs_trip.append(x_from)
        ys_trip.append(y_from)
        xs_trip.append(x_to)
        ys_trip.append(y_to)

def import_trips_from_od(etree):
    root = etree.getroot()
    # Se for um trips.xml, não tem a tag links.
    for trip_xml in root.findall('trip'):
        #global simulator_nodes
        global xs_trip
        global ys_trip

        # Para quando as coordenadas vem dos nós do simulador.
        node_id_from = trip_xml.attrib['origin']
        node_id_to = trip_xml.attrib['destination']
        x_from, y_from = simulator_nodes[node_id_from]
        x_to, y_to = simulator_nodes[node_id_to]
        
        # Para quando as coordenadas vem dos pontos da OD
        #x_from = float(trip_xml.attrib['od_coord_o_x'])
        #y_from = float(trip_xml.attrib['od_coord_o_y'])
        #x_to = float(trip_xml.attrib['od_coord_d_x'])
        #y_to = float(trip_xml.attrib['od_coord_d_y'])

        xs_trip.append(x_from)
        ys_trip.append(y_from)
        xs_trip.append(x_to)
        ys_trip.append(y_to)


def main():
    # Arquivo gerado pelo Victor (São Paulo inteira), já contendo ciclovias.
    #map_tree = ET.parse('../geracao-network/1-import-pontos-sampa-from-openStreetMaps/labeled_network.xml')
    #map_tree = ET.parse('../geracao-network/2-import-infraestrutura-cicloviaria-from-geosampa/map-sp2.xml') # arquivo fornecido pelo Eduardo (retângulo de são paulo)
    #map_tree = ET.parse('../geracao-network/1-map-convertion/mapa-do-eduardo.xml') # arquivo fornecido pelo Eduardo (retângulo de são paulo)
    #import_simulator_nodes(map_tree)

    #map_tree = ET.parse('map-over85.xml')
    #import_map_nodes(map_tree)

    map_tree = ET.parse('../geracao-network/1-map-convertion/network.xml')
    import_map_nodes(map_tree)

    # Arquivo gerado pelo notebook "import-bike-trips" quando "add_od_coords"
    # é setado True. 
    #trip_tree = ET.parse('od-coords-bike-trips.xml')

    # Arquivo apenas com as viagens de bike que devem ser importadas para o simulador.
    trip_tree = ET.parse('od-bike-trips-sp-only.xml')
    import_trips_from_od(trip_tree)

    print('len xs_map: ', len(xs_map))
    print('len xs_map: ', len(xs_map))
    print('len ys_trip: ', len(xs_trip))
    print('len ys_trip: ', len(ys_trip))

    # Coordenadas do retângulo que recorta 85% das viagens dentro de SP.
    #lim_n=-23.508136
    #lim_s=-23.669957
    #lim_w=-46.755948
    #lim_e=-46.431446

    
    print('max long: ', max(xs_map))
    print('min long: ', min(xs_map))
    print('max lat: ', max(ys_map))
    print('min lat: ', min(ys_map))


    plt.scatter(xs_map, ys_map, color='blue', s=1, label='pontos do mapa')
    plt.scatter(xs_trip, ys_trip, color='red', s=1, label='pontos origen e de destino')
    #rect = plt.Rectangle((lim_w, lim_s), lim_e -lim_w, lim_n - lim_s, fill=False)
    #plt.gca().add_patch(rect)
    plt.legend()
    plt.show()


if __name__== "__main__":
    main()
