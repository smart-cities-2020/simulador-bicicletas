#!/bin/python3

import xml.etree.ElementTree as ET
from xml.dom.minidom import parse, parseString
import pyproj
import sys
import os


def beautifyXML(root_element_object):
    import xml.dom.minidom as minidom
    from io import BytesIO
    buf = BytesIO()
    buf.write(ET.tostring(root_element_object))
    buf.seek(0)
    root = minidom.parse(buf)
    return root.toprettyxml(indent=' '*4)

def save_prettify_xml(root_element, output_file_path):
    root_element.write('temp.xml')
    #prettify_xml('temp.xml', 'output-network-with-cicleways-and-cyclelanes.xml')
    dom1 = parse('temp.xml')
    text_file = open(output_file_path, 'w')
    text_file.write(dom1.toprettyxml(indent=' '*4))
    text_file.close()    
    os.remove('temp.xml')


def print_to_file(file_name, string):
    text_file = open(file_name, "w")
    text_file.write(string)
    text_file.close()


def import_simulator_nodes(etree):
    root = etree.getroot()
    for node_xml in root.find('nodes').findall('node'):
    
        x = float(node_xml.attrib['x'])
        y = float(node_xml.attrib['y'])
        # Convertendo das coordenadas do simulador para lat,long
        lat, longg = pyproj.transform('EPSG:32719', 'wgs84', x, y)

        node_xml.set('x', str(longg))
        node_xml.set('y', str(lat))


def main():

    if (len(sys.argv) < 3):
        print("usage: convert-map-to-lat-long.py <input_file_name> <output_file_name>")
        return 1
    root = ET.parse(sys.argv[1])
    import_simulator_nodes(root)

    xml_name = sys.argv[2]
    #xml_string = beautifyXML(root)
    #print_to_file(xml_name, xml_string)

    save_prettify_xml(root, xml_name)


if __name__== "__main__":
    main()
