map=output-network-with-cicleways-and-cyclelanes.xml
nos=`grep node $map | wc -l`
links=`grep link $map | wc -l`
cicleways=`grep 'cycleway="true' $map | wc -l`
cyclelanes=`grep 'cyclelane="true' $map | wc -l`
cycleways_and_cyclelanes=`grep 'cycleway="true' $map | grep 'cyclelane="true' | wc -l`
echo "$nos nós"
echo "$links links, dos quais"
echo " $cicleways são ciclovias"
echo " $cyclelanes são ciclofaixas"
echo " $cycleways_and_cyclelanes são ciclofaixa *e* ciclovia (não deveria acontecer)"
