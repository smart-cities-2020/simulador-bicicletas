# pip3 install geopandas
import geopandas as gpd
import pandas
import shapely
import sys
import xml.etree.ElementTree as ET
import math
import matplotlib.pyplot as plt
import numpy as np
import pyproj
import os
from xml.dom.minidom import parse, parseString
from shapely.geometry import LineString, Point
from math import radians, degrees, sin, cos, asin, acos, sqrt

this = sys.modules[__name__]
#this.tree = ET.parse('map-sp2.xml') # arquivo fornecido pelo Eduardo (retângulo de são paulo)
this.tree = ET.parse('../1-map-convertion/network.xml')
this.nodes_simulador = {} # id => (x, y)
this.edges_simulador = [] # guarda xml_node
this.xs_mapa = []
this.ys_mapa = []
this.xs_ciclovias_geosampa = []
this.ys_ciclovias_geosampa = []
this.xs_ciclofaixas_geosampa = []
this.ys_ciclofaixas_geosampa = []
this.xs_ciclofaixas = [] # resultado
this.ys_ciclofaixas = []
this.xs_ciclovias = [] # resultado
this.ys_ciclovias = []
this.gdf = None
this.pts_ciclofaixas_geosampa = None
this.pts_ciclovias_geosampa = None
this.LIMIAR = 30 # em metros 

# Estratégia
# Para cada link do mapa, verificar se há uma ciclofaixa perto, se tiver marca que tem ciclofaixa.
# Pra saber se tem ciclofaixa perto do link: 
# pega os nós do link do mapa, para cada nó do mapa verifica qual o nó de ciclofaixa mais próximo;
# se ambas as distâncias (nó mapa - nó ciclofaixa mais próxima -- dos dois nós do link) forem menores que um limiar (1km?) então marca que é ciclofaixa.
#
# Obs: quando falarmos em "mapa", entenda-se o mapa do simulador (map.xml)

# Erros
# Porcentagem de erros (falsos negativos) para ciclofaixas: 5 %
# Porcentagem de erros (falsos negativos) para ciclovias: 5 %

# Resultado do cq.sh:
# 170577 nós
# 363446 links, dos quais
#  3480 são ciclovias
#  8143 são ciclofaixas
#  380 são ciclofaixa *e* ciclovia (não deveria acontecer)

def main():

    print('Importando dados')
    import_nodes_do_simulador()
    import_links_do_simulador()
    open_geosampa()
    percorre_geosampa()

    print('Iniciando processamento dos links')
    # percorre os links do mapa 
    for link_xml in this.edges_simulador:
        process_link(link_xml)

    # imprimie gráfico 
    plt.scatter(this.xs_mapa, this.ys_mapa, color='blue', s=1, label='pontos do mapa')
    plt.scatter(this.xs_ciclofaixas_geosampa, this.ys_ciclofaixas_geosampa, color='red', s=20, label='ciclofaixas no geosampa')
    plt.scatter(this.xs_ciclovias_geosampa, this.ys_ciclovias_geosampa, color='orange', s=20, label='ciclovias no geosampa')
    plt.scatter(this.xs_ciclofaixas, this.ys_ciclofaixas, color='yellow', s=2, label='ciclofaixas ajustadas ao mapa')
    plt.scatter(this.xs_ciclovias, this.ys_ciclovias, color='green', s=2, label='ciclovias ajustadas ao mapa')
    plt.legend()
    plt.show()

    # imprime XML (igual ao mapa original, mas marcando as ciclofaixas)
    print('Escrevendo novo mapa para o simulador (com ciclofaixas) em output-network-with-cicleways-and-cyclelanes.xml')
    this.tree.write('temp.xml')
    prettify_xml('temp.xml', 'network.xml')
    os.remove('temp.xml')
    print('Novo mapa escrito')

    controle_de_qualidade()




def prettify_xml(input_file_path, output_file_path):
    dom1 = parse(input_file_path)
    text_file = open(output_file_path, 'w')
    text_file.write(dom1.toprettyxml(indent=' '*4))
    text_file.close()    

def import_nodes_do_simulador():
    root = tree.getroot()
    for node_xml in root.find('nodes').findall('node'):
        node_id = node_xml.attrib['id']
        x = float(node_xml.attrib['x'])
        y = float(node_xml.attrib['y'])
#        lat, longg = pyproj.transform('EPSG:32719', 'wgs84', x, y) # se fosse converter pra lat, long
        this.nodes_simulador[node_id] = (x, y)
        this.xs_mapa.append(x)
        this.ys_mapa.append(y)

def import_links_do_simulador():
    root = tree.getroot()
    for link_xml in root.find('links').findall('link'):
        this.edges_simulador.append(link_xml)

def open_geosampa():
    this.gdf = gpd.read_file('SAD69-96_SHP_redecicloviaria/SAD69-96_SHP_redecicloviaria.shp')
    this.gdf.crs = '+init=epsg:29193' # contando pro geopands a projeção utilizada pelo geosampa
    # http://processamentodigital.com.br/2013/07/27/lista-dos-codigos-epsg-mais-utilizados-no-brasil/
    #proj="+init=epsg:32719" # projeção do simulador 
    proj="+init=epsg:4326" # projeção com lat long
    this.gdf = gdf.to_crs(proj) # converte sistema de projeção
    gdf_ciclofaixas = gdf[gdf['rc_descric'] == 'CICLOFAIXA']
    this.pts_ciclofaixas_geosampa = gdf_ciclofaixas.geometry.unary_union
    gdf_ciclovias = gdf[gdf['rc_descric'] == 'CICLOVIA']
    this.pts_ciclovias_geosampa = gdf_ciclovias.geometry.unary_union

def percorre_geosampa():
    # Percorre pontos do geosampa só pra adicionar
    # os pontos em xs_ciclofaixas e ys_ciclofaixas 
    # pra poder printar o traçado original (geosampa) das ciclofaixas
#    for tipo_via, geometry in this.gdf.head(10)[['rc_descric', 'geometry']].values: 
    for tipo_via, geometry in this.gdf[['rc_descric', 'geometry']].values: 
        if tipo_via == 'CICLOFAIXA':
            if type(geometry) == shapely.geometry.linestring.LineString:
                for coords in geometry.coords:
                    x_geosampa, y_geosampa = list(coords)
                    this.xs_ciclofaixas_geosampa.append(x_geosampa)
                    this.ys_ciclofaixas_geosampa.append(y_geosampa)
            if type(geometry) == shapely.geometry.multilinestring.MultiLineString:
                for via in geometry:
                    for coords in via.coords:
                        x_geosampa, y_geosampa = list(coords)
                        this.xs_ciclofaixas_geosampa.append(x_geosampa)
                        this.ys_ciclofaixas_geosampa.append(y_geosampa)
        if tipo_via == 'CICLOVIA':
            if type(geometry) == shapely.geometry.linestring.LineString:
                for coords in geometry.coords:
                    x_geosampa, y_geosampa = list(coords)
                    this.xs_ciclovias_geosampa.append(x_geosampa)
                    this.ys_ciclovias_geosampa.append(y_geosampa)
            if type(geometry) == shapely.geometry.multilinestring.MultiLineString:
                for via in geometry:
                    for coords in via.coords:
                        x_geosampa, y_geosampa = list(coords)
                        this.xs_ciclovias_geosampa.append(x_geosampa)
                        this.ys_ciclovias_geosampa.append(y_geosampa)

def process_link(link_xml):
    link_id = link_xml.attrib['id']
    node_id_from = link_xml.attrib['from']
    node_id_to = link_xml.attrib['to']
    x_from, y_from = this.nodes_simulador[node_id_from]
    x_to, y_to = this.nodes_simulador[node_id_to]
    point_from = Point(x_from, y_from)
    point_to = Point(x_to, y_to)
    #
    # para ciclofaixa
    dist1 = distancia_para_ciclofaixa(point_from)
    dist2 = distancia_para_ciclofaixa(point_to)    
    if dist1 < this.LIMIAR and dist2 < this.LIMIAR:
        print('Link %s tem ciclofaixa' % link_id)
        link_xml.attrib['cyclelane'] = "true" # marca link como ciclofaixa
        link_xml.attrib['cycleway'] = "false" # marca que não é ciclovia, pois dados do geosampa prevalecem sobre os dados do streetmaps
        this.xs_ciclofaixas.append(x_to)
        this.ys_ciclofaixas.append(y_to)
    else:
        link_xml.attrib['cyclelane'] = "false" # marca link como ciclofaixa
    #
    # para ciclovia
    dist1 = distancia_para_ciclovia(point_from)
    dist2 = distancia_para_ciclovia(point_to)    
    if dist1 < this.LIMIAR and dist2 < this.LIMIAR:
        print('Link %s tem ciclovia' % link_id)
        link_xml.attrib['cycleway'] = "true" # marca link como ciclofaixa
        this.xs_ciclovias.append(x_to)
        this.ys_ciclovias.append(y_to)
    else:
        # se streemap falou que tinha ciclovia, mantemos como ciclovia,
        # considerando o streetmaps como informação complementar
        pass


def distancia_para_ciclofaixa(point):
    nearst_point = shapely.ops.nearest_points(point, this.pts_ciclofaixas_geosampa)[1]
    return distancia_entre_pontos_latlong_em_metros(point, nearst_point)

def distancia_para_ciclovia(point):
    nearst_point = shapely.ops.nearest_points(point, this.pts_ciclovias_geosampa)[1]
    return distancia_entre_pontos_latlong_em_metros(point, nearst_point)

def distancia(x1, y1, x2, y2):
    return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def distancia_entre_pontos_latlong_em_metros(p1, p2):
    lon1, lat1, lon2, lat2 = map(radians, [p1.y, p1.x, p2.y, p2.x])
    try:
        return 6371000 * (
            acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1 - lon2))
        )
    except ValueError:
        # Em alguns casos, dá um erro por conta da distância muito pequena
        return 1

def controle_de_qualidade():
    # ciclofaixa
    # Percorre os pontos do geosampa e verifica se tem ciclofaixa por perto no mapa gerado para o simulador
    #
    # Primeiro joga todos os pontos do mapa com ciclofaixa numa coleção única
    print('Calculando controle de qualidade')
    pts_com_ciclofaixa = LineString(zip(this.xs_ciclofaixas, this.ys_ciclofaixas))
    #
    total = len(this.xs_ciclofaixas_geosampa)
    erros = 0
    for x, y in zip(this.xs_ciclofaixas_geosampa, this.ys_ciclofaixas_geosampa):
        point = Point(x, y)
        nearst_point = shapely.ops.nearest_points(point, pts_com_ciclofaixa)[1]
        distance = distancia_entre_pontos_latlong_em_metros(point, nearst_point)
        if distance > this.LIMIAR:
            erros += 1
    print('Porcentagem de erros (falsos negativos) para ciclofaixas:', math.trunc(1.0*erros/total*100), '%')
    #
    # ciclovia
    pts_com_ciclovia = LineString(zip(this.xs_ciclovias, this.ys_ciclovias))
    total = len(this.xs_ciclovias_geosampa)
    erros = 0
    for x, y in zip(this.xs_ciclovias_geosampa, this.ys_ciclovias_geosampa):
        point = Point(x, y)
        nearst_point = shapely.ops.nearest_points(point, pts_com_ciclovia)[1]
        distance = distancia_entre_pontos_latlong_em_metros(point, nearst_point)
        if distance > this.LIMIAR:
            erros += 1
    print('Porcentagem de erros (falsos negativos) para ciclovias:', math.trunc(1.0*erros/total*100), '%')
    #
    print('Um erro (false negativo) é um ponto do geosampa sem ponto do mapa com ciclofaixa por perto')
    print('I.e.: no gráfico, ponto vermelho sem amarelo por cima')
    # Obs: um ponto amarelo sem vermelho por baixo também seria um erro (falso positivo),
    # mas por inspeção visual no mapa já vimos que isso parece não acontecer.


if __name__== "__main__":
    main()

