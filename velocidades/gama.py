from random import random
import math
import matplotlib.pyplot as plt

""" Algorithm 1 of http://home.iitk.ac.in/~kundu/paper120.pdf"""
def generalized_gama_sample(mean, std_dev, alpha):
    s = random()
    sinal = math.cos(2 * math.pi * s)
    u = random()
    return mean + std_dev * sinal * 2 * math.log(1 - math.pow(u, 1/alpha))



def test():

    sample_size = 500
    mean = 10
    std_dev = 2
    alpha = 2

    numbers = [ generalized_gama_sample(mean, std_dev, alpha) for i in range(1, sample_size) ]
    plt.hist(numbers, bins = 10)
    plt.show()


if __name__== "__main__":
    test()

