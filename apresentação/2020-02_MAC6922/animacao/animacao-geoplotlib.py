#!/bin/python3

# Obtido de geoplotlib.ipynb

#GeoplotLib
#
#Mobility trace for the city of São Paulo
#URL: http://interscity.org/open_data/
#ZIP FILE: https://www.dropbox.com/s/hatzbujfspte12i/car-bus-simulation.zip?dl=0#

# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import geoplotlib

pd.set_option("display.precision", 15)
pd.set_option('precision', 15)
# CSV DATASET from InterSCimulator
TRACE_DATA = '../../../analise/od_clean_bike_events_lat_lon.csv'
COLUMNS = ['time', 'type', 'vid', 'link', 'vehicle', 'action', 'lat', 'lon']

# Define input size number of rows to be read from the csv into the pandas dataframe
UNITY=1
VID='4858_52'#'6311_1'
SMALL=10000
MEDIUM=5000000
LARGE=None
#INPUT_SIZE=SMALL
INPUT_SIZE=LARGE

mdf = pd.read_csv(TRACE_DATA, nrows=INPUT_SIZE, names=COLUMNS, delimiter=",", header=None, float_precision="high")
       
# All data points from a given vehicle id
def filter_by_id(dataframe, vid):
    return dataframe.loc[dataframe['vid'] == vid]

# The first time a vehicle id registered an action in the simulation
def origin(dataframe,id):
    return filter_by_id(dataframe, id).iloc[[0]]
        
# The last time a vehicle id registered an action in the simulation
def destiny(dataframe, id):
    return filter_by_id(dataframe, id).iloc[[-1]]

tool_tip = lambda x: 'time: ' + str(x["time"]) + ', lat: ' + str(x["lat"]) + ', lon: ' + str(x["lon"])







# Custom layer
from geoplotlib.layers import BaseLayer
from geoplotlib.layers import HotspotManager
from geoplotlib.utils import BoundingBox
from geoplotlib.core import BatchPainter
import random
import time

trajectories = mdf

class DotDensityLayer(BaseLayer):

    def __init__(self, data, color=None, point_size=2, f_tooltip=None):
        """Create a dot density map
        :param data: data access object
        :param color: color
        :param point_size: point size
        :param f_tooltip: function to return a tooltip string for a point
        """
        self.frame_counter = 3600
        self.data = data
        self.last = self.data.drop_duplicates(subset=['vid'], keep='first')
        #self.last = data
        self.color = color
        if self.color is None:
            self.color = [255,0,0]
        self.point_size = point_size
        self.f_tooltip = f_tooltip

        self.hotspots = HotspotManager()


    def invalidate(self, proj):
        self.last = self.data.loc[self.data['time'] == self.frame_counter]
        lon_at = self.last['lon']
        lat_at = self.last['lat']
        
        self.x, self.y = proj.lonlat_to_screen(lon_at, lat_at)
        


    def draw(self, proj, mouse_x, mouse_y, ui_manager):
        self.painter = BatchPainter()
        current = self.data.loc[self.data['time'] == self.frame_counter]
        #print(current.head(2))
        self.last = pd.concat([self.last, current]).drop_duplicates(subset='vid', keep='last')
        lon_at = self.last['lon']
        lat_at = self.last['lat']
        
        x, y = proj.lonlat_to_screen(lon_at, lat_at)
        
        if self.f_tooltip:
            for i in range(0, len(x)):
                record = {k: self.data[k][i] for k in self.data.keys()}
                self.hotspots.add_rect(x[i] - self.point_size, y[i] - self.point_size,
                                       2*self.point_size, 2*self.point_size,
                                       self.f_tooltip(record))

        self.painter.set_color(self.color)
        self.painter.points(x, y, 2*self.point_size, False)
        
        self.painter.batch_draw()
        picked = self.hotspots.pick(mouse_x, mouse_y)
        if picked:
            ui_manager.tooltip(picked)
        self.frame_counter += 1
        #time.sleep(0.4)


    def bbox(self):
        return BoundingBox.from_points(lons=self.data['lon'], lats=self.data['lat'])
        
geoplotlib.add_layer(DotDensityLayer(trajectories))
geoplotlib.show()
