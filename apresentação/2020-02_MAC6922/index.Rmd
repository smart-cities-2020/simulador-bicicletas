---
title: "MAC6922 - Tópicos Avançados de Pesquisa em Cidades Inteligentes"
subtitle: "<BR> Simulador de bicicletas" 
author: "<BR> Grupo: Haydée Svab, Karlson Bezerra, Leonardo Leite, Marcelo Schmitt e Victor Moura"
encoding: "UTF-8"
output:
  xaringan::moon_reader:
    lib_dir: libs
    chakra: libs/remark-latest.min.js
    css: [default, "css/metropolis.css", "css/metropolis-fonts.css", "css/footer.css"]
    includes:
      after_body: html/insert-logo.html
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
# Para exportar em PDF: pagedown::chrome_print("index.html", timeout = 600)
# knit: pagedown::chrome_print

# #88398a
---
layout: true
  
<div class="my-footer"><span>

<a> Simulador de bicicletas</a>

</span></div> 
---
class:

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)

knitr::opts_chunk$set(fig.align = "center", message=FALSE, warning=FALSE)

library(tidyverse)

```

# Questão

## <center><b>Como a simulação computacional pode apoiar a gestão pública na avaliação de planos cicloviários?</b></center> 

```{r, echo=FALSE, out.width="30%"}
knitr::include_graphics("https://media.giphy.com/media/3o84Ufy4oR2l0nSdEs/giphy.gif")
```

<BR>
Como fatores como inclinação, presença/ausência de infraestrutura cicloviária e a ocorrência de acidentes influenciam as viagens de bicicleta?
<!-- Como utilizar simulação computacional pode dar suporte às tomadas de decisão para termos cada vez mais políticas públicas orientadas por dados? -->

---
class:

# Abordagem

```{r echo=FALSE, fig.cap="<center><b> </center></b>", out.width="100%"}
knitr::include_graphics("img/ESQUEMA.png")
```

---
class:

# Fontes de dados e Tecnologias

## Fontes de dados
- Simulador de viagens: Inter-simulator [doutorado de Eduardo Felipe Zambom Santana]
- Viagens (todos os modos, um dia típico): Pesquisa Origem Destino 2017 [Metrô-SP]
- Viagens de bicicleta : operação de bikesharing de jan/2018 a dez/2019 [BikeSampa]
- Infraestrutura cicloviária existente: mapas com ciclovias e ciclofaixas [GeoSampa]
- Infraestrutura cicloviária futura: plano cicloviário 2028 [CET]
- Acidentes de trânsito: acidentes, veículos e vítimas [CET]
- Grafo com infraestrutura cicloviária [Open Street Maps]

## Tecnologias
- Erlang [modelo de agentes para simulador]
- Python (pandas, geopandas, numpy, geoplotlib) [maps, trips]
- R (pré-processamento) [velocidades, acidentes]


---
class:

# Velocidades
- Definição: velocidade = distância euclidiana entre origem e destino da viagem¹/duração da viagem
- Eliminação de outliers: excluídos valores inferiores ao LI² e superiores ao LS² do boxplot
- Flutuação horária descartada pois amostra de viagens de bicicleta é pequena
<br> (1.279 observações representando ~ 380 mil viagens)

```{r echo=FALSE, out.width="50%"}
knitr::include_graphics("img/boxplot_velocidades_bike.png")
```

.footnote[
<i>¹ Da OD2017, que fornece apenas coordenadas de início e fim das viagens, não fornece trajeto/rota</i>
<br> <i>² Limite inferior LI = Q1 - 1,5.AIQ e Limite superior LS = Q3 + 1,5.AIQ </i>
]

---
class:

# Velocidades

```{r echo=FALSE, fig.cap="<center><b> </center></b>", out.width="65%"}
knitr::include_graphics("img/vel_viagens.jpg")
```

---
class:

# Velocidades

```{r echo=FALSE, fig.cap="<center><b> </center></b>", out.width="64%"}
knitr::include_graphics("img/vel_viagens_destaque.jpg")
```

---
class:

# Velocidades
- Análise da distribuição das viagens -> distribuição Gama generalizada
- Média = 7,441 km/h | Desvio padrão = 3,204148 km/h

```{r echo=FALSE, fig.cap="<center> Histograma de velocidades das viagens de bicicleta</center>", out.width="50%"}
knitr::include_graphics("img/hist_viagens_bikes.png")
```

- <b> Implementação da distribuição gama generalizada no simulador não é trivial </b>
- Geração de 400mil valores de velocidade a partir da média e do desvio padrão, segundo a distribuição gama -> arquivo para modelo
- Cálculo de fatores multiplicadores para a inclinação e infraestrutura cicloviária a partir de dados de bikesharing -> parâmetros para modelo

---
class:

# Acidentes
- Identificação se acidentes ocorreram na presença ou na ausência de infraestrtuura cicloviária
- <b> Viagens são feitas por pessoas! </b>
<br> (1 acidente com 1 ônibus tem 25 vítimas -> seriam 25 viagens ocorrendo)

##### <center> Cálculo da probabilidade de ser vítima de acidente de trânsito <br> num deslocamento em São Paulo, durante a semana, no ano de 2017 </center>

- Consideradas vítimas de acidentes marcado se ocorreu na presença/ausência de infra
- Definição: quantidade média de vítimas por hora / quantidade de viagens por hora [de 1 dia de semana típico]
- Segmentação pela presença/ausência de infraestrutura cicloviária

<center> qtde_vitimas_hora_com_infra / qtde_viagens_com_infra </center>
<br> <center> qtde_vitimas_hora_sem_infra / qtde_viagens_sem_infra </center> 

---
class:

# Acidentes

### <center> Como definir a quantidade de viagens que ocorreram na presença/ausência de infraestrutura cicloviária? </center>

<br>
- Formatado o output para que marcasse se uma viagem passou ou não por infraestrutura cicloviária
- Formatado o output para que marcasse se uma viagem teve mais de 50% passando por infraestrutura cicloviária
- Feita a simulação das viagens de carro, constatou-se que em <b> ambos os outputs todas a viagens passavam por pelo menos 50% de vias com infraestrutura cicloviária </b>
<br>
- Foi adotada a proporção³ de vias com tratamento cicloviário (504 km) sobre o total de vias pavimentadas (17,2mil km) da cidade de São Paulo


.footnote[
<i>³ Isto implicar assumir a simplificação de que as vias são homogêneas (com capacidade e outras características iguais). </i>
]

---
class:

# Maps

## Geração do mapa da simulação

### 1. Links 
### 2. Infraestrutura cicloviária
### 3. Altitude 

---
class:

# Maps

### 1. Links

- Mapa com coordenadas inconversíveis
- "Mapa do Eduardo" (apenas um recorte da cidade)

<BR>
<center> <b> E ... queríamos a cidade toda! </b></center>

```{r, echo=FALSE, out.width="30%"}
knitr::include_graphics("https://media.giphy.com/media/qBiMcID6S5kac/giphy.gif")
```

---
class:

# Maps

## 1. Links

### Abordagem 1: Open Street Maps

- Sem filtros: arquivos eram inviáveis de se trabalhar.
- Com filtros: não foi possível importar todas as vias.
- <b> Problemas de conectividade </b> dos grafos importados.
- Investimos bastante tempo nessa frente.

```{r, echo=FALSE, out.width="25%"}
knitr::include_graphics("https://tenor.com/view/disney-moana-pig-sad-eyes-gif-7539569.gif")
```

---
class:

# Maps

## 1. Links

```{r echo=FALSE, fig.cap="<center><b> Mapa gerado com o Overpass que contém 85% das viagens de bike de SP </center></b>", out.width="65%"}
knitr::include_graphics("img/overpass85-centrado.png")
```

---
class:

# Maps

## 1. Links

```{r echo=FALSE, fig.cap="<center><b> Mapa do Overpass continha nós em construções </center></b>", out.width="60%"}
knitr::include_graphics("img/nos_em_construcoes.png")
```

---
class:

# Maps

## 1. Links

### Abordagem 2: "Mapa do Eduardo"

* Ficamos com o mapa do Eduardo
* Era totalmente conexo!
* 118.882 nós e 50.365 links (unidirecionais)
* Convertemos o mapa do Eduardo para lat|lon
* No final percebemos: mapa contempla 87% das viagens de bike da OD


---
class:

# Maps

## 2. Infraestrutura cicloviária

### Abordagem 1: Open Street Maps
- Premissa: grafo já viriam com indicação da infraestrutura cicloviária
- Descartado devido aos problemas de conectividade

### Abordagem 2: GeoSampa
- Premissa: melhor confiabilidade da informação (dados oficiais)
- Problema: coordenadas do Geosampa não coincidem com as coordenadas do "mapa do Eduardo"
- Utilizado algoritmo de aproximação: 
  - se o link está muito próximo da infraestrutura cicloviária, então deve ser um link de infraestrutura cicloviária

---
class:

# Maps

## 2. Infraestrutura cicloviária

```{r echo=FALSE, fig.cap="<center><b> Mapa com a infraestrutura cicloviária que poderia ser utilizada </center></b>", out.width="64%"}
knitr::include_graphics("img/ciclofaixas-mapeadas-limiar-30-metros-5porcento-de-erro.png")
```

---
class:

# Maps

## 2. Infraestrutura cicloviária

```{r echo=FALSE, fig.cap="<center><b> Mapa com a infraestrutura cicloviária importada </center></b>", out.width="60%"}
knitr::include_graphics("img/infraestrutura-mapa-eduardo.png")
```

---
class:

# Maps

## 3. Altitude 

- Dados obtidos pelo Eduardo por meio de API 
  - CSV mapeando ID do nó para altitude <BR>
- Incorporamos esses dados no mapa (xml) <BR>
- Com as altitudes é possível definir se há declive ou aclive, o que influencia a velocidade do agente


---
class:

# Maps

## Mapa final
- Altitudes adicionadas aos nós
- Informação sobre infraestrutura cicloviária adicionada aos links
  - Ciclovia ou ciclofaixa, para infra existente

## Não importado
- Infra futura 
  (na base fornecida no plano cicloviário de 2028 não era indicada a distinção entre ciclovias e ciclofaixas)

---
class:

# Trips

## Reaproveitamento das trips existentes

### Adição das trips de bicicletas com base na OD2017
- Aproximação de lat/lon para nós do grafo no formato do simulador
- Viagens expandidas (truncado para inteiro)
- Viagens consideradas dentro de uma área reduzida que engloba 87% das viagens de bike de SP
- Somente consideradas viagens dentro do município de São Paulo

### Dificuldades
- Executar simulações com as viagens fornecidas (viagens sem bicicletas)

---
class:

# Trips

## Área desejada - premissas:
- Viagens dentro do município de São Paulo
- Cobertura de pelo menos 85% das viagens

```{r echo=FALSE, fig.cap="<center><b> </center></b>", out.width="64%"}
knitr::include_graphics("img/od-bike-trip-and-sp-85box.png")
```


---
class:

# Trips

## Área de estudo

```{r echo=FALSE, fig.cap="<center><b> </center></b>", out.width="64%"}
knitr::include_graphics("img/area_estudo.jpg")
```

---
class:

# Modelo

## Inputs
- Modo "bike" no arquivo de input do simulador
- Novo agente no simulador: o agente bicicleta

## Como considera velocidades

- Adoção de freespeed (velocidade base) a velocidade média a partir do BikeSampa: 7,44km/h
- Adoção de fatores multiplicadores para a inclinação e infraestrutura cicloviária com base no BikeSampa
- Distribuição de velocidades com base nos dados da OD2017

---
class:

# Modelo

## Como considera acidentes
- No início da viagem, o agente bicicleta calculará a probabilidade de acidente da sua viagem da seguinte forma:
prob_acidente_agente_viagem = (dist_percurso_com_infra/dist_percurso_total) \* prob_acidente_com_infra + (dist_percurso_sem_infra/dist_percurso_total) \* prob_acidente_sem_infra
- Ao final, a probabildiade é distribuída igualmente nos links do percurso: prob_acidente_agente_link = prob_acidente_agente_viagem / qtde_links_total_viagem

## Interação entre agentes
- O comportamento do agente bicicleta influencia o agente carro (capacidade da via)
- O comportamento do agente carro influencia o agente bicicleta (capacidade da via)

---
class:

# Implementação no simulador

## Modelo de velocidade

* Para cada agente bike: 
  * sorteia uma "personal speed" da distribuição gama
  * distribuição baseada nos dados da OD
  * mas com média dos dados do bike sampa
  * agente atualiza quantidade de bikes no link
  * e também conta a bike na ocupação total da via
    * 1 carro = 5 bikes, 1 ônibus = 3 carros

---
class:

# Implementação no simulador

* Em cada link agente define sua velocidade:

    Freespeed = get_free_speed_for_bike(...),
    Speed = speed_for_bike_considering_traffic(...),

```{r echo=FALSE, fig.cap="<center><b> Cálculo do freespeed usando fatores de declividade e infra cicloviária </center></b>", out.width="64%"}
knitr::include_graphics("img/free_speed.png")
```

---
class:

# Implementação no simulador

* speed_for_bike_considering_traffic
 * utiliza a curva para carros já utilizada antes no simulador
 * para ciclovias/faixas considera só bikes
 * se link estiver saturado, velocidade = 1 m/s
 * capacidade de ciclovia: CicleLinkCapacity = Length / (BikeLength + SafetySpace),

```{r echo=FALSE, fig.cap="<center><b> Cálculo da velocidade do ator considerando o tráfego </center></b>", out.width="64%"}
knitr::include_graphics("img/traffic_speed.png")
```

---
class:

# Alterações no simulador

Linhas alteradas:
1543 ++  708 --

Arquivos alterados:
- src/class_Bike.erl
- src/class_Bus.erl
- src/class_Car.erl
- src/class_CarManager.erl
- src/class_Street.erl
- src/create_scenario.erl
- src/map_parser.erl
- src/map_parser_test.erl
- src/personal_speed_distribution_for_bikes.csv
- src/run_map_parser_tests.sh
- src/run_traffic_models_tests.sh
- src/small-network.xml
- src/traffic_models.erl
- src/traffic_models_test.erl

---
class:

# Realização
```{r echo=FALSE, fig.cap="<center><b> </center></b>", out.width="100%"}
knitr::include_graphics("img/ESQUEMA-FINAL.png")
```

---
class:

# Eventos - output

```{r echo=FALSE, fig.cap="<center><b> Output do simulador com os eventos de uma pequena demonstração</center></b>", out.width="60%"}
knitr::include_graphics("img/eventos-demo.png")
```

- Hora e Minuto
- Tempo da Simulação
- Evento (start, move, arrival)
- Nome do agente
- Posição
- Tempo total [segundos]
- Distância total [metros]
- Presença/ausência de infraestrutura cicloviária em algum trecho da viagem
- Presença/ausência de infraestrutura cicloviária em mais de 50% da viagem
<!-- - Probabilidade de ser vítima de acidente [Não implementada] -->

---
class:

# Limitações

- Velocidades calculadas a partir da distância euclidiana <BR>
- Utilização da OD2017 considerando apenas Modo Principal <BR>
- Ao adotar a proporção de vias com tratamento cicloviário sobre o total de vias pavimentadas da cidade de São Paulo no cálculo das probabildiades de acidentes, assume-se que as vias são homogêneas (com capacidade e outras características iguais)

---
class:

# Trabalhos futuros
- Implementar um modelo de acidentes
- Aprimorar a forma de definição de rota (melhores caminhos ou invés de menor caminho)
- Rever forma de expansão das viagens (não truncada)
- Considerar multimodalidade nas hora de definir os parâmetros do modelo
- Rever o cálculo da probabilidade de acidentes melhorando a simplificação assumida quanto à quantidade de viagens que passam por vias com infraestrutura ciloviária
- Obter um grafo conexo que cubra a cidade de São Paulo inteira e, quiçá, a Região Metropolitana
- Prever agentes para demais modos sobre pneus, como ônibus e táxis (convencionais e carros por aplicativo)
- Prever agentes específicos para pedestres (responsáveis por quase 1/3 das viagens totais da RMSP)






