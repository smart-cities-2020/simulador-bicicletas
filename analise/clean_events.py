#!/bin/python3
# Este script separa os eventos de saída do simulador em dois arquivos:
# 1) eventos em formato xml
# 2) eventos em formato csv
#
# Além disso, ele adiciona uma raíz ao arquivo xml, tornando-o compatível
# para ser usado como entrada para o script events_xml_to_csv.py.

import sys

def main():
    if (len(sys.argv) < 3):
        print("usage: clean_events.py <input_file_name> <output_file_name>")
        return 1

    xml_lines = []
    csv_lines = []

    with open(sys.argv[1]) as f: 
        for line in f:
            if line.startswith('<'):
                xml_lines.append(line)
            else:
                csv_lines.append(line)

    xml_file = sys.argv[2] + ".xml"
    with open(xml_file, "w") as f:
        f.write("<events>")
        for line in xml_lines:
            f.write(line)
        f.write("</events>")
    
    csv_file = sys.argv[2] + ".csv"
    with open(csv_file, "w") as f:
        for line in csv_lines:
            f.write(line)


if __name__== "__main__":
    main()
