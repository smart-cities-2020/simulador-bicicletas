#!/bin/python3
# Este script converte os eventos de saída do simunaldor do formato xml
# para o formato csv e adiciona informação de lat, long aos eventos para
# que possa ser feita uma animação usando os arquivos em 
# ../apresentação/2020-02_MAC6922/animacao.
# É esperado que o xml de entrada tenha o seguinte formato:
#
#<events>
#<event time="7" type="actend" ... />
#<event time="7" type="departure" ... />
#...
#<event time="7" type="PersonEntersVehicle" ... />
#</events>

import sys
from xml.dom.minidom import parse, parseString
import xml.etree.ElementTree as ET
import csv

# Sim, variáveis globais
simulator_nodes = {} # id => (x, y) # (x é lon, y é lat)
simulator_links = {} # id => (node_origin)
xs_sim = []
ys_sim = []
xs_trip = []
ys_trip = []


def import_links_do_simulador(etree):
    root = etree.getroot()
    # Se for um network.xml, buscar pela tag links primeiro.
    for link_xml in root.find('links').findall('link'):
        global simulator_links
        node_id_from = link_xml.attrib['from']
        simulator_links[link_xml.attrib['id']] = node_id_from


def import_map_nodes(etree):
    root = etree.getroot()
    for node_xml in root.find('nodes').findall('node'):
        global simulator_nodes
        node_id = node_xml.attrib['id']
        x = float(node_xml.attrib['x'])
        y = float(node_xml.attrib['y'])
        simulator_nodes[node_id] = (x, y)


def convert_to_csv():
    root = ET.parse(sys.argv[1])
    header = root.find('event').attrib.keys()
    #print('header: ', header)
    #for k in header:
    #    print('key ', k)
    
    with open(sys.argv[2], 'w') as f:
        writer = csv.writer(f)
        # Imprime o cabeçalho
        #writer.writerow(header)
        for tag in root.iter():
            if not tag.attrib.get('link'):
                continue

            row = []
            for key in tag.attrib.keys():
                row.append(tag.attrib[key])

            origin_node = simulator_nodes[simulator_links[tag.attrib.get('link')]]
            row.append(origin_node[1]) # latitude
            row.append(origin_node[0]) # longitude
            #print(row)
            writer.writerow(row)
        f.close()


def main():
    if (len(sys.argv) < 3):
        print("usage: events_xml_to_csv.py <input_file_name> <output_file_name>")
        return 1

    map_tree = ET.parse('../geracao-network/1-map-convertion/network.xml')
    import_map_nodes(map_tree)

    import_links_do_simulador(map_tree)

    convert_to_csv()

    return 0


if __name__== "__main__":
    main()
